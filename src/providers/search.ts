import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { TokenProvider } from './token';
import { GlobalVars } from './globalVars';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
 
@Injectable()
export class SearchProvider {

    constructor(public http: HttpClient, private token: TokenProvider, private globals: GlobalVars) {}

    search(preferences: string, headers?: HttpHeaders | null): Observable<any> {
        const expandedHeaders = this.token.prepareHeader(headers);
        return this.http.get<any>(this.globals.getBaseUrl() + '/search/' + preferences, expandedHeaders)
          .timeout(15000)
          .map(data => data.search);
    }

    highlights(register: {categories?: string, latitude: number, longitude: number} | null, headers?: HttpHeaders | null): Observable<any> {
        const expandedHeaders = this.token.prepareHeader(headers);
        return this.http.post<any>(this.globals.getBaseUrl() + '/highlights', register, expandedHeaders)
          .timeout(15000)
          .map(data => data.search);
    }

    //converter para POST apos terminado
    showitems(data: {addid: string, ofeid: number}, headers?: HttpHeaders | null): Observable<any> {
        const expandedHeaders = this.token.prepareHeader(headers);
        return this.http.post<any>(this.globals.getBaseUrl() + '/showitems', data, expandedHeaders)
          .timeout(15000)
          .map(data => data.search);
    }
}