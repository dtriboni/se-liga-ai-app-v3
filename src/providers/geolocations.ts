import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './globalVars';
import 'rxjs/add/operator/timeout';

import 'rxjs/add/operator/map';
import { TokenProvider } from './token';

@Injectable()

export class GeolocationsProvider {

  constructor(public http: HttpClient, private token: TokenProvider, private globals: GlobalVars) { }

  geolocations(coordinates: {latitude: number, longitude: number}, headers?: HttpHeaders): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.post<any>(this.globals.getBaseUrl() + '/geolocations', coordinates, expandedHeaders)
    .timeout(15000)
      .map(data => data.geolocations);
  }


}
