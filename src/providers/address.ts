import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/timeout';

import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './globalVars';
import { TokenProvider } from './token';

@Injectable()

export class AddressProvider {
 
  constructor(public http: HttpClient, private token: TokenProvider, private globals: GlobalVars) { }

  address(register: { 
                      address: string, 
                      number: string, 
                      complement: string, 
                      zipcode: string, 
                      neighboor: string, 
                      city: string, 
                      state: string,
                      whatsapp: string,
                      tel1: string,
                      tel2: string,
                      latitude: string, 
                      longitude: string,
                      type: number }, headers?: HttpHeaders | null) {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http
      .post<any>(this.globals.getBaseUrl() + '/address', register, expandedHeaders)
      .timeout(15000)
      .do(res => {
           
      });
  }

  updateAddress(register: { 
                    uadid: number,
                    address: string, 
                    number: string, 
                    complement: string, 
                    zipcode: string, 
                    neighboor: string, 
                    city: string, 
                    state: string,
                    whatsapp: string,
                    tel1: string,
                    tel2: string,
                    latitude: string, 
                    longitude: string,
                    type: number }, headers?: HttpHeaders | null) {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http
      .put<any>(this.globals.getBaseUrl() + '/update-address', register, expandedHeaders)
      .timeout(15000)
      .do(res => {

    });
  }

  checkaddresses(headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/checkaddresses', expandedHeaders)
    .timeout(15000)
      .map(data => data.numaddresses);
  }

  getaddresses(headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/getaddresses', expandedHeaders)
    .timeout(15000)
      .map(data => data.addresses);
  }

  getaddress(uadID: number, headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/getaddress/' + uadID, expandedHeaders)
    .timeout(15000)
      .map(data => data.address);
  }

  removeaddress(addid: number = 0, headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.delete<any>(this.globals.getBaseUrl() + '/removeaddress/' + addid, expandedHeaders)
    .timeout(15000)
      .map(data => data.addresses);
  }

}
