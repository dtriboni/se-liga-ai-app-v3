import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './globalVars';
import { TokenProvider } from './token';
import 'rxjs/add/operator/timeout';

import 'rxjs/add/operator/map';

@Injectable()

export class ProductsProvider {
 
  constructor(private token: TokenProvider, private http: HttpClient, private globals: GlobalVars) { }

  product(credentials: {ean: string}, headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http
      .post<any>(this.globals.getBaseUrl() + '/products', credentials, expandedHeaders)
      .timeout(15000)
      .map(data => data.product[0]);
  }
}
