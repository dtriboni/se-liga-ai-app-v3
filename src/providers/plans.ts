import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './globalVars';
import { TokenProvider } from './token';
import 'rxjs/add/operator/timeout';

import 'rxjs/add/operator/map';

@Injectable()

export class PlansProvider {

  constructor(private token: TokenProvider, public http: HttpClient, private globals: GlobalVars) { }

  plans(strid: string, headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/plans/' + strid, expandedHeaders)
      .timeout(15000)
      .map(data => data.plans);
  }

}
