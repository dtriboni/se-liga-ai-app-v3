import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './globalVars';
import 'rxjs/add/operator/timeout';

import 'rxjs/add/operator/map';
import { TokenProvider } from './token';

@Injectable()

export class LotteryProvider {

  constructor(public http: HttpClient, private token: TokenProvider, private globals: GlobalVars) { }

  lottery(id: number, headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/lottery/' + id, expandedHeaders)
    .timeout(15000)
      .map(data => data.lottery[0]);
  }

  savelottery(register: {lotid: number, email: string, number: string}, headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.post<any>(this.globals.getBaseUrl() + '/savelottery', register, expandedHeaders)
    .timeout(15000)
    .do(res => {
           
    });
  }


}
