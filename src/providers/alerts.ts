import {Injectable} from '@angular/core';
import { AlertController } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { GlobalVars } from './globalVars';

@Injectable()

export class Alerts {
  
  constructor(public alertCtrl: AlertController, public localNotifications: LocalNotifications, private globals: GlobalVars) {
    //this.localNotifications.on('click').subscribe(notification => {
    //  let json = JSON.parse(notification.data);
    //  alert(JSON.stringify);
    //});  
  }

  public showNotification(nText, location) {
      this.localNotifications.schedule({
        title: 'Se Liga ' + localStorage.getItem('reduce-name') + '!',
        text: nText,
        icon: 'https://em2d.com.br/seligaai/icons/logo/logoLocalNotification.png',
        data: {location: location},
        led: 'FF0000',
        vibrate: true,
        sound: null
      });
  }

}