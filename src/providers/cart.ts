import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './globalVars';
import { TokenProvider } from './token';

import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

@Injectable()

export class CartProvider {

  constructor(public http: HttpClient, private token: TokenProvider, private globals: GlobalVars) { }

  shipping(item: any, headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.post<any>(this.globals.getBaseUrl() + '/shipping', item, expandedHeaders)
      .timeout(15000)
      .map(data => data);
  }

  coupon(cod: any, headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.post<any>(this.globals.getBaseUrl() + '/coupon', cod, expandedHeaders)
      .timeout(15000)
      .map(data => data);
  }

  coupons(strid: number = 0, headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/coupons/' + strid, expandedHeaders)
      .timeout(15000)
      .map(data => data);
  }

  fund(headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/wallet', expandedHeaders)
      .timeout(15000)
      .map(data => data);
  }

  extract(headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/walletextract', expandedHeaders)
      .timeout(15000)
      .map(data => data.wallet);
  }

  orders(headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/orders', expandedHeaders)
      .timeout(15000)
      .map(data => data.orders);
  }

}
