import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './globalVars';
import { TokenProvider } from './token';

@Injectable()

export class OfferProvider {
 
  constructor(public http: HttpClient, private token: TokenProvider, private globals: GlobalVars) { }

  getoffer(ofeID: number = 0, headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/getoffer/' + ofeID, expandedHeaders)
      .timeout(15000)
      .map(data => data.offer);
  }

  getoffers(headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/getoffers', expandedHeaders)
      .timeout(15000)
      .map(data => data.offer);
  }
}
