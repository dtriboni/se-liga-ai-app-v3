import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/timeout';

import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './globalVars';
import { TokenProvider } from './token';

@Injectable()

export class AuthProvider {
 
  constructor(public http: HttpClient, private token: TokenProvider, private globals: GlobalVars) { }

  login(credentials: {user: string, pwd: string}, headers?: HttpHeaders) {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http
      .post<any>(this.globals.getBaseUrl() + '/login', credentials, expandedHeaders)
      .timeout(15000)
      .do(res => {
          localStorage.setItem('usr-name', res.name);
          localStorage.setItem('usr-email', res.email);
          localStorage.setItem('usr-datanasc', res.datanasc);
          localStorage.setItem('usr-gender', res.gender);
          localStorage.setItem('usr-cpfcnpj', res.cpfcnpj);
          localStorage.setItem('reduce-name', res.name.split(" ", 1));
          localStorage.setItem('logged', res.signup);
          localStorage.setItem('login-info', JSON.stringify(res));
      });
  }

  remember(email: string, headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/remember/' + email, expandedHeaders)
      .timeout(15000)
      .map(data => data.code);
  }

  savepassword(pass: {user: string, pwd: string}, headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.put<any>(this.globals.getBaseUrl() + '/savepassword', pass, expandedHeaders)
      .timeout(15000)
      .map(data => data.password);
  }

  userinfo(headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/user-info', expandedHeaders)
      .timeout(30000)
      .map(data => data.userinfo[0]);
  }

  logout(): Observable<any> {
    return this.http
      .get(this.globals.getBaseUrl() + '/logout')
      .timeout(15000)
      .do(data => {});
  }

}
