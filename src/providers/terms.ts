import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './globalVars';
import 'rxjs/add/operator/timeout';

import 'rxjs/add/operator/map';
import { TokenProvider } from './token';

@Injectable()

export class TermsProvider {

  constructor(public http: HttpClient, private token: TokenProvider, private globals: GlobalVars) { }

  terms(headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/terms', expandedHeaders)
    .timeout(15000)
      .map(data => data.terms[0]);
  }


}
