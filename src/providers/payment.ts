import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalVars } from './globalVars';
import { TokenProvider } from './token';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { AlertController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { App } from 'ionic-angular/components/app/app';
import 'rxjs/add/operator/timeout';
import { HomePage } from '../pages/home/home';
import { AuthProvider } from './auth';


declare var PagSeguroDirectPayment: any;

@Injectable()
export class PaymentProvider {
  
  public credencial: Credencial;
  public dados = new Dados();

  constructor(private http: HttpClient, 
              private storage: Storage, 
              public navCtrl: App,
              public loadingCtrl: LoadingController,
              private datepipe: DatePipe,
              private toastCtrl: ToastController,
              private authProvider: AuthProvider, 
              private varGlobais: GlobalVars, 
              private token: TokenProvider) {}


  // MÉTODO QUE DISPARA OUTROS MÉTODOS NECESSÁRIOS PARA A UTILIZAÇÃO DA API DO PAGSEGURO
  iniciar(email, token) 
  { 
    this.credencial = new Credencial();
    this.dados = new Dados();

    this.credencial.key = this.datepipe.transform(new Date(), "ddMMyyyyHHmmss");
    this.credencial.email = email;
    this.credencial.token = token;
   
    if (!this.varGlobais.getStatusScript()) 
    {
      let loading = this.loadingCtrl.create({ spinner: 'crescent', content: 'Aguarde...'});
      loading.present();

      this.getSession(email, token).subscribe(data => {

        PagSeguroDirectPayment.setSessionId(data);
        this.credencial.idSession = data;
        this.storage.set('credencial', this.credencial);
        loading.dismiss();

      }, error => {

        loading.dismiss();
        this.presentToast('Estamos em manutenção em nossos meios de pagamento. Por favor tente novamente mais tarde!');

      }) ;
    }
  }
  
  // RETORNA A SESSÃO QUE VAI SER UTILIZADA PELA API
  // ESTE É UM ID QUE É GERADO PELA API DO PAGSEGURO PARA FAZER O 
  // CONSUMO PARA CONCRETIZAR A TRANSAÇÃO
  getSession(email, token, headers?: HttpHeaders): Observable<any> 
  {
      this.getCartContentToOrder();
      const expandedHeaders = this.token.prepareHeader(headers);
      return this.http.get<any>(this.varGlobais.getBaseUrl() + '/ps-session/' + email + '/' + token, expandedHeaders)
      .timeout(15000)
      .map(data => data.id); 
  }


  // RETORNA O STATUS DO PAGAMENTO PELA API DE GATEWAY
  statusPayment(email, token, headers?: HttpHeaders): Observable<any> 
  {
      const expandedHeaders = this.token.prepareHeader(headers);
      return this.http.get<any>(this.varGlobais.getBaseUrl() + '/status-payment/' + email + '/' + token, expandedHeaders)
      .timeout(15000)
      .map(data => data.status_payment); 
  }

  // RETORNA OS MEIOS DE PAGAMENTO DISPONÍVEIS NA CONTA PARA EXIBIÇÃO NO CHECKOUT
  // BUSCA A BANDEIRA DO CARTÃO (EX: VISA, MASTERCARD ETC...) E DEPOIS BUSCA AS PARCELAS;
  // ESTA FUNÇÃO É CHAMADA QUANDO O INPUT QUE RECEBE O NÚMERO DO CARTÃO PERDE O FOCO;
  buscaBandeira(value, parcelas) 
  { 
    PagSeguroDirectPayment.setSessionId(this.credencial.idSession);

    PagSeguroDirectPayment.getBrand(
    {
        cardBin: this.dados.numCard,

        success: response => {

          this.dados.bandCard = response.brand.name;
          this.buscaParcelas(value, parcelas); 
        },
        error: response => {

          this.presentToast('Estamos em manutenção em nossos meios de pagamento. Por favor tente novamente mais tarde!');
        }
    });
  }

  // VERIFICA QUAL BANDEIRA FOI INFORMADA PELO CLIENTE AO DIGITAR OS DADOS DO CARTÃO E RETORNA AS 
  // PARCELAS DISPONPIVEIS E VAI BUSCAR AS PARCELAS NA API DO PAGSEGURO PARA O CLIENTE ESCOLHER  
  buscaParcelas(value, parcelas) 
  { 
    PagSeguroDirectPayment.getInstallments(
    {
        amount: value,                //valor total da compra (deve ser informado)
        brand: this.dados.bandCard,   //bandeira do cartão (capturado na função buscaBandeira)
        maxInstallmentNoInterest: (parcelas == 1 ? 2 : parcelas), 
        
        success: response => {

          var numParc: Array<Object> = [];
          for (var p = 0; p < response.installments[this.dados.bandCard].length; p++){
            if (parcelas >= response.installments[this.dados.bandCard][p].quantity){
              numParc[p] = response.installments[this.dados.bandCard][p];
            }
          }
          this.dados.parcelas = numParc;
          //this.dados.parcelas = response.installments[this.dados.bandCard];
        },
        error: response => { 

          this.presentToast('Estamos em manutenção em nossos meios de pagamento. Por favor tente novamente mais tarde!');
        }
    });
  }

  // INICIA OS PROCESSOS PARA QUE SEJA REALIZADO O PAGAMENTO
  // AO CLICAR NO BOTÃO PAGAR
  pagar(transaction: string = '') 
  {
    let loading = this.loadingCtrl.create({ spinner: 'crescent', content: 'Processando Pagamento...'});
    loading.present();

    this.dados.hashComprador = PagSeguroDirectPayment.getSenderHash();

    if (this.dados.type_payment == 'boleto' || this.dados.type_payment == 'wallet')
    {

      loading.dismiss();
      this.enviaDadosParaServidor(transaction);

    }else{

        PagSeguroDirectPayment.createCardToken(
        {
          
            cardNumber:       this.dados.numCard,
            cvv:              this.dados.codSegCard,
            expirationMonth:  this.dados.mesValidadeCard,
            expirationYear:   this.dados.anoValidadeCard,
            brand:            this.dados.bandCard,

            success: response => {

              loading.dismiss();
              this.dados.hashCard = response.card.token;
              this.enviaDadosParaServidor(transaction);

            },error: response => { 

              loading.dismiss();
              this.presentToast('Não foi possível processar seu pagamento agora. Tente novamente mais tarde.');
            }
        });
    }
  }

  // DISPARA OUTROS MÉTODOS PARA PODER CONSUMIR A API DO PAGSEGURO E CONCRETIZAR A TRANSAÇÃO
  // BUSCA DADOS DO PAGADOR
  enviaDadosParaServidor(transaction) 
  {
    let loading = this.loadingCtrl.create({ spinner: 'crescent', content: 'Aguarde...'});
    loading.present();

    this.authProvider.userinfo().subscribe(data_client => {

      this.dados.email_ps = this.credencial.email;
      this.dados.token_ps = this.credencial.token;

      this.dados.cliente = data_client.cliente;
      this.dados.telefone = data_client.tel1;
      this.dados.email = data_client.email;
      if (this.dados.cpf == '00000000191')
        this.dados.cpf = data_client.cpfcnpj.replace(/[^\d]+/g,'');;
      this.dados.nascimento = data_client.nascimento;
      this.dados.logradouro = data_client.address;
      this.dados.numero = data_client.number;
      this.dados.bairro = data_client.neighboor;
      this.dados.cep = data_client.zipcode;
      this.dados.cidade = data_client.city;
      this.dados.estado = data_client.state;

      this.pagarCheckoutTransp(transaction, this.dados).subscribe(result => {

          // IR PARA TELA DE SUCESSO DO PEDIDO
          loading.dismiss();

          var complMsg = "";
          if (this.dados.type_payment == 'boleto'){
            complMsg = "Um boleto foi encaminhado para " + this.dados.email + ".";
          }else{
            complMsg = "";
          }

          this.storage.clear();
          this.varGlobais.get();
          this.presentToast('Seu pedido foi processado com êxito. ' + complMsg);
          
          let nav = this.navCtrl.getRootNav();

          if (transaction == "wallet")
            nav.setRoot(HomePage, {}); 
          else
            nav.setRoot(HomePage, {newped: true});   

      } , error => { 
          loading.dismiss();
          this.presentToast('Não foi possível processar seu pagamento agora.');
      });

    }, error => {

      loading.dismiss();
      this.presentToast('Ops! Parece que você está sem endereço marcado para entrega. Selecione um endereço no seu cadastro!');
      let nav = this.navCtrl.getRootNav();
      nav.setRoot(HomePage, {}); 

    });
  }

  getCartContentToOrder()
  {
    this.dados.item = [];
    setTimeout( () => {
      return this.storage.forEach( (value: string, key: string, iterationNumber: Number) => {
        if (value !== undefined){
          var only_cart_keys = key.split("-", 1);
          if (only_cart_keys[0] === "c" || only_cart_keys[0] === "o"){
            var json = JSON.parse(JSON.stringify(value));
            this.dados.item.push(json);
          }
        }
      })
      .then(() => { 
        return Promise.resolve(this.dados.item);
      })
      .catch((error) => {
        return Promise.reject(error);
      });
    }, 200);
  }

 
  /**
   * Method to show Toast messages 
   * on bottom of device
   * @param msgToast 
   */
  presentToast(msgToast) 
  {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 5500,
      position: 'bottom'
    });
    toast.present();
  }

  // MÉTODO QUE FAZ O CONSUMO COM TODOS OS DADOS NECESSÁRIOS PELA API 
  // COM A FINALIDADE DE EFETUAR O PAGAMENTO
  /**
   * 
   * @param dados 
   * @param headers 
   */
  private pagarCheckoutTransp (transaction, dados:Dados, headers?: HttpHeaders | null)
  { 
    const expandedHeaders = this.token.prepareHeader(headers);            
    let body = JSON.stringify({ dados });
    return this.http.post(this.varGlobais.getBaseUrl() + '/' + transaction, body, expandedHeaders)
      .timeout(30000)
      .map(res => res);
  }  
}

// CLASSE PARA ARMAZENAR NOSSOS DADOS DE ACESSO A CONTA DO PAGSEGURO
/**
 * 
 */
export class Credencial {
  key: string;
  urlPagSeguroDirectPayment: string;
  urlTransacao: string;
  idSession: string;
  email: string;
  token: string;
  isSandBox: boolean;
}

// CLASSE PARA ARMAZENAR OS DADOS DA TRANSAÇÃO DE CHECKOUT NECESSÁRIOS PARA CONSUMIR A API
export class Dados {
  public id: number;
  public email_ps: string = '';
  public token_ps:string = '';
  public nome: string = '';
  public cliente: string = '';
  public telefone: string = '';
  public email: string = '';
  public cpf: string = '';
  public nascimento: string = '';
  public logradouro: string = '';
  public numero: string = '';
  public bairro: string = '';
  public cep: string = '';
  public cidade: string = '';
  public estado: string = '';
  
  public numCard: string = '4111111111111111';
  public mesValidadeCard: string = '12';
  public anoValidadeCard: string = '2030';
  public codSegCard: string = '123'; 

  public hashComprador: string; 
  public bandCard: string;    
  public hashCard: string;           
  public parcelas: Array<Object> = []; 

  public type_payment: string = 'CreditCard';

  public numparcela: string = '';
  public valparcela: string = '';
  
  public item = [];

  public ord_value: number = 0.00;
  public couid: number = 0;
  public coupon: number = 0.00;
  public logid: number = 0;
  public shipping: number = 0.00;

  constructor(obj?) {
    Object.assign(this, obj, {}, {});
  }
}