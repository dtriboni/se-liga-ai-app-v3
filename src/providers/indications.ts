import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/timeout';

import 'rxjs/add/operator/do';
import { GlobalVars } from './globalVars';
import { TokenProvider } from './token';

@Injectable()

export class IndicationProvider {
 
  constructor(public http: HttpClient, private token: TokenProvider, private globals: GlobalVars) { }

  indication(register: { name: string, tel: string}, headers?: HttpHeaders | null) {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http
      .post<any>(this.globals.getBaseUrl() + '/indication', register, expandedHeaders)
      .timeout(15000)
      .do(res => {
         
      });
  }
}
