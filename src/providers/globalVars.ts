import {Injectable} from '@angular/core';
import { Storage } from '@ionic/storage';
import { CartProvider } from './cart';
import { ValueTransformer } from '@angular/compiler/src/util';
import { Clipboard } from '@ionic-native/clipboard';
import { ToastController } from 'ionic-angular';

@Injectable()

export class GlobalVars {
  
  public status: boolean = false;
  public networkDown: boolean = false;
  public isUserLogged: boolean = false;
  public showFooterOnSomePages: boolean = true;
  public isHomePage: boolean = false;

  public baseUrl =  "https://em2d.com.br/seligaai/apihomolog/v2";
  public appIdOneSignal = "21233d99-be0b-415e-8d6a-7eaf6f73e407";
  public googleOneSignalID = "709548536063";

  public pagseg_email = "seligaaiapp@gmail.com";
  public pagseg_token = "82DC8E8575E64552AC53F4C004DBC1BE";
  
  public latitude: number = 0;
  public longitude: number = 0;
  public newlatlng: boolean = false;

  public itemOnCart: boolean = false;
  public total_cart: number = 0.00;
  public item_cart: string = "Seu carrinho está vazio!";
  
  public arrCart = [];
  public cart_items = [];

  constructor(private storage: Storage, private toastCtrl: ToastController) { 
    //this.storage.clear();
    this.get(); 
  }

  getBaseUrl() {
    return this.baseUrl;
  }

  setStatusScript(status: boolean) {
    this.status = status;
  }

  getStatusScript() {
    return this.status;
  }

  delQtd(id){
    var objJson = {
      id: "",
      ofeid: 0,
      store: "",
      stock: 0,
      description: "",
      caracteristic: "",
      ctsid: 0,
      vlunit: 0.00,
      qtd: 0
    };
    this.storage.get(id).then((val) => {
      var currQtd = val.qtd;
      if (currQtd > 1){
        objJson.id = id;
        objJson.ofeid = val.ofeid;
        objJson.store = val.store;
        objJson.stock = val.stock;
        objJson.description = val.description;
        objJson.caracteristic = val.caracteristic;
        objJson.ctsid = val.ctsid;
        objJson.vlunit = val.vlunit;
        objJson.qtd = (currQtd -1); 
        this.storage.remove(id);
        this.storage.set(id, JSON.parse(JSON.stringify(objJson)));
        this.get();
      }  
    });
  }

  addQtd(id){
    var objJson = {
      id: "",
      ofeid: 0,
      store: "",
      stock: 0,
      description: "",
      caracteristic: "",
      ctsid: 0,
      vlunit: 0.00,
      qtd: 0
    };
    this.storage.get(id).then((val) => {
      var currQtd = val.qtd;
      if (currQtd < val.stock){
        objJson.id = id;
        objJson.ofeid = val.ofeid;
        objJson.store = val.store;
        objJson.stock = val.stock;
        objJson.description = val.description;
        objJson.caracteristic = val.caracteristic;
        objJson.ctsid = val.ctsid;
        objJson.vlunit = val.vlunit;
        objJson.qtd = (currQtd +1); 
        this.storage.remove(id);
        this.storage.set(id, JSON.parse(JSON.stringify(objJson)));
        this.get();
      }  
    });
  }

  addToCart(str: any, stock: number, desc: string, ofeid: number, vlUnit: any, arrQtd: any, arrCarac: any){
    var objJson = {
      id: "",
      ofeid: 0,
      store: "",
      stock: 0,
      description: "",
      caracteristic: "",
      ctsid: 0,
      vlunit: 0.00,
      qtd: 0
    };
    this.arrCart = [];
    arrQtd.forEach((value, i) => {
      this.storage.remove("c-" + i);
      if (value > 0){
        objJson.id = "c-" + i;
        objJson.ofeid = ofeid;
        objJson.store = str[i];
        objJson.stock = stock;
        objJson.description = desc;
        objJson.caracteristic = arrCarac[i];
        objJson.ctsid = i;
        objJson.vlunit = vlUnit[i];
        objJson.qtd = value; 
        this.arrCart.push(JSON.parse(JSON.stringify(objJson))); 
      }
    });
  }

  addInCart(btn){
    if (this.arrCart.length == 0 && btn == "ADICIONAR")
    {
      this.presentToast("Primeiro, selecione a quantidade desejada!");
    }else{
      this.arrCart.forEach((value, i) => {
        this.save(value.id, JSON.parse(JSON.stringify(value)));
      });
      this.get();
      this.openCart();
    }
  }

  addOneItem(str: string, desc: string, vlunit: number = 0.00, ofeid: number){
    var objJson = {
      id: "",
      ofeid: 0,
      store: "",
      stock: 0,
      description: "",
      caracteristic: "",
      ctsid: 0,
      vlunit: 0.00,
      qtd: 0
    };
    objJson.id = "o-" + ofeid;
    objJson.ofeid = ofeid;
    objJson.store = str;
    objJson.stock = 9999;
    objJson.description = desc;
    objJson.caracteristic = null;
    objJson.ctsid = 0;
    objJson.vlunit = vlunit;
    objJson.qtd = 1;
    this.save("o-" + ofeid, objJson);
    this.get();
    this.openCart();
  }

  removeItem(id){
    var layerBgContent = document.getElementById('itm-' + id);
    layerBgContent.classList.add('animated');
    layerBgContent.classList.add('fadeOut');
    setTimeout( () => {
      this.remove(id);
    }, 700);  
  }

  save(key: string, arrCart: any) {
    this.storage.set(key, arrCart); 
  }

  remove(key: string) {
    this.storage.remove(key);
    console.warn('One Item removed: ' + key);
    this.get();
  }

  get(){
    setTimeout( () => {
      this.cart_items = [];
      var c = 0;
      var tot = 0;
      var vlitem = "Seu carrinho está vazio!";
      return this.storage.forEach( (value: string, key: string, iterationNumber: Number) => {
        if (value !== undefined){
          var only_cart_keys = key.split("-", 1);
          if (only_cart_keys[0] === "c" || only_cart_keys[0] === "o"){
            c++;
            var json = JSON.parse(JSON.stringify(value));
            tot += (json.qtd * json.vlunit);
            vlitem = (c > 1 ? c + " ítens no carrinho" : "Um ítem no carrinho");
            this.cart_items.push(json);
          }
        }
      })
      .then(() => {
        this.total_cart = tot; 
        this.item_cart = vlitem;
        if (c > 0){
          this.itemOnCart = true;
        }else{
          this.closeCart();
          this.itemOnCart = false;
        }  
        return Promise.resolve(this.cart_items);
      })
      .catch((error) => {
        return Promise.reject(error);
      });
    }, 200);
  }

  openCart(){
    if (this.itemOnCart == true)
    {
      var layerCartContent = document.getElementById('cartcontent');
      var layerBgContent = document.getElementById('bgcart');
      layerCartContent.classList.remove('cart-close');
      layerBgContent.classList.remove('cart-bg-close');
      layerCartContent.classList.add('cart-open');
      layerBgContent.classList.add('cart-bg-open');
    }
  }
  closeCart(){
    if (this.itemOnCart == true)
    {
      var layerCartContent = document.getElementById('cartcontent');
      var layerBgContent = document.getElementById('bgcart');
      layerCartContent.classList.remove('cart-open');
      layerBgContent.classList.remove('cart-bg-open');
      layerCartContent.classList.add('cart-close');
      layerBgContent.classList.add('cart-bg-close');
    }
  }

  presentToast(msgToast) {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 5000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

}