// util.ts
import {Injectable} from '@angular/core';
import { HttpHeaders } from '../../node_modules/@angular/common/http';

@Injectable()
export class TokenProvider {
  
    private api_key: string;

    prepareHeader(headers: HttpHeaders | null): object {
        var val = localStorage.getItem('login-info');
        if (val != null){
          const key = JSON.parse(val);
          this.api_key = key.sla_api_key;
        }else{
          this.api_key = "iKinQppQiHc3aho2ctBvq1S7J4e1V7nIv1ZQJokuM34=";
        }  
        headers = headers || new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json');
        headers = headers.set('Accept', 'application/json');
        headers = headers.set('sla-api-key', this.api_key);
        return {
          headers: headers
        }
      }
}