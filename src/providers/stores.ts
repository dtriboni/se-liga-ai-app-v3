import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './globalVars';
import { TokenProvider } from './token';

import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

/* export interface StoresProvider {
  userId: number;
  id: number;
  title: string;
  body: string;
} */

@Injectable()

export class StoresProvider {

  constructor(public http: HttpClient, private token: TokenProvider, private globals: GlobalVars) { }

  store(storeID: number = 0, headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/store/' + storeID, expandedHeaders)
      .timeout(15000)
      .map(data => data.store);
  }

  branches(storeID: number = 0, headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/branches/' + storeID, expandedHeaders)
      .timeout(15000)
      .map(data => data.branches);
  }

  banners(headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/store-banners', expandedHeaders)
      .timeout(15000)
      .map(data => data.banners);
  }

  /* stores(coordinates: {latitude: number, longitude: number}, page: number = 1, catID: number = 0, headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return Observable.defer(() => {
      return this.http.post<any>(new GlobalVars().getBaseUrl() + '/stores/' + page + '/'+ catID, coordinates, expandedHeaders)
        .delay(1000)
        .timeout(15000)
        .map(data => data);
    });
  } */

  stores(coordinates: {latitude: number, longitude: number}, page: number = 1, catID: number = 0, headers?: HttpHeaders | null): Observable<any> {
    
    const expandedHeaders = this.token.prepareHeader(headers);
      return this.http.post<any>(this.globals.getBaseUrl() + '/stores/' + page + '/'+ catID, coordinates, expandedHeaders)
        .timeout(15000)
        .map(data => data);
  }

  sellers(coordinates: {latitude: number, longitude: number}, page: number = -1, catID: number = 0, headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
      return this.http.post<any>(this.globals.getBaseUrl() + '/sellers/' + page + '/'+ catID, coordinates, expandedHeaders)
        .timeout(15000)
        .map(data => data);
  }

  favourites(data: {latitude: number, longitude: number, stores: string}, headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.post<any>(this.globals.getBaseUrl() + '/favourites', data, expandedHeaders)
      .timeout(15000)
      .map(data => data.stores);
  }

  storeinfo(headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/store-info', expandedHeaders)
      .timeout(30000)
      .map(data => data.storeinfo[0]);
  }

}
