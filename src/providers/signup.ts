import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/timeout';

import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './globalVars';
import { TokenProvider } from './token';

@Injectable()

export class SignupProvider {
 
  constructor(public http: HttpClient, private token: TokenProvider, private globals: GlobalVars) { }

  signup(register: {name: string, 
                    gender: string, 
                    signup: string, 
                    cpfcnpj: string,
                    datanasc: string,
                    email: string, 
                    pwd: string}, headers?: HttpHeaders) {
    const expandedHeaders = this.token.prepareHeader(headers);     
    return this.http
      .post<any>(this.globals.getBaseUrl() + '/signup', register, expandedHeaders)
      .timeout(15000)
      .do(res => {
          localStorage.setItem('usr-name', res.name);
          localStorage.setItem('usr-email', res.email);
          localStorage.setItem('usr-datanasc', res.datanasc);
          localStorage.setItem('usr-cpfcnpj', res.cpfcnpj);
          localStorage.setItem('usr-gender', res.gender);
          localStorage.setItem('reduce-name', res.name.split(" ", 1));
          localStorage.setItem('logged', register.signup);
          localStorage.setItem('login-info', JSON.stringify(res));
      });
  }

  updatesignup(register: {name: string, 
                          gender: string,
                          signup: string, 
                          email: string, 
                          cpfcnpj: string,
                          datanasc: string,  
                          pwd: string}, headers?: HttpHeaders | null): Observable<any> {
      const expandedHeaders = this.token.prepareHeader(headers);
          return this.http
          .patch<any>(this.globals.getBaseUrl() + '/updatesignup', register, expandedHeaders)
          .timeout(15000)
          .do(res => {
            localStorage.setItem('usr-name', res.name);
            localStorage.setItem('usr-email', res.email);
            localStorage.setItem('usr-datanasc', res.datanasc);
            localStorage.setItem('usr-cpfcnpj', res.cpfcnpj);
            localStorage.setItem('usr-gender', res.gender);
            localStorage.setItem('reduce-name', res.name.split(" ", 1));
            localStorage.setItem('logged', register.signup);
            localStorage.setItem('login-info', JSON.stringify(res));
          });

       
  }


  checkemail(email: string = '', headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http
      .get<any>(this.globals.getBaseUrl() + '/checkemail/' + email, expandedHeaders)
      .timeout(15000)
      .map(res => res.numemail);
  }

}
