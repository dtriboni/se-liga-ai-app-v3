import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GlobalVars } from './globalVars';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { TokenProvider } from './token';

@Injectable()

export class CategoriesProvider {

  constructor(public http: HttpClient, private token: TokenProvider, private globals: GlobalVars) { }

  categories(headers?: HttpHeaders | null): Observable<any> {
    const expandedHeaders = this.token.prepareHeader(headers);
    return this.http.get<any>(this.globals.getBaseUrl() + '/categories', expandedHeaders)
      .timeout(15000)
      .map(data => data.categories);
  }

}
