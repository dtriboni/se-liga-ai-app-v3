import { ErrorHandler } from '@angular/core';
import { IonicErrorHandler } from 'ionic-angular';
import { HttpErrorResponse } from '@angular/common/http';

export class AppErrorHandler extends IonicErrorHandler implements ErrorHandler {

    constructor() { 
        super();
    }

    handleError(errorResponse: HttpErrorResponse | any): void {
        console.log('Error: ' + errorResponse);
        super.handleError(errorResponse);
    }

}