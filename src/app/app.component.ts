import { Component } from '@angular/core';
import { Platform, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation'; 
import { Network } from '@ionic-native/network';
import { GlobalVars } from '../providers/globalVars';
import { OneSignal, OSNotificationPayload } from '@ionic-native/onesignal';
import { ToastController } from 'ionic-angular';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { CategoriesProvider } from '../providers/categories';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { PagamentoPage } from '../pages/pagamento/pagamento';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {

  rootPage:any = HomePage;

  setCategoriesEnabled: string = null;
  
  constructor(private network: Network, 
              public globals: GlobalVars,
              private oneSignal: OneSignal,
              public geolocation: Geolocation,
              private toastCtrl: ToastController,
              private launchNavigator: LaunchNavigator,
              public categoriesProvider: CategoriesProvider,
              public app: App, 
                      platform: Platform,
                      statusBar: StatusBar, 
                      splashScreen: SplashScreen) {

    if (platform.is('cordova')) 
    {

      /**
       * Check if platform is ready to start
       * application and features.
       */
      platform.ready().then(() => {
        statusBar.overlaysWebView(false);
        statusBar.backgroundColorByHexString('#356083');
        localStorage.removeItem('notifications');
        this.categoriesProvider.categories().subscribe(data => 
        {
          this.setCategoriesEnabled = localStorage.getItem('alerts');
            var catEnabled: string = null;
            data.forEach(function (cat) 
            {
              var item = JSON.parse(JSON.stringify(cat));
              catEnabled += ',' + item.icon;
            });
            localStorage.setItem('alerts', catEnabled+'');
        },
        error => {});


        /**
         * Show Toast and NotConnection View if
         * Internet connections was down.
         */
        this.network.onDisconnect().subscribe(() => {
          if (!this.globals.networkDown)
          {
            //show NotConnection View
            this.globals.networkDown = true;
            this.globals.showFooterOnSomePages = false;
          }
        });

        
        /**
         * Show Toast and disable NotConnection View if
         * Internet connections is restabilished.
         */
        this.network.onConnect().subscribe(() => {
          if (this.globals.networkDown)
          {
            // hide NotConnection View
            this.presentToast('Opa, você está online novamente!');
            this.globals.networkDown = false;
          }
        });
        

        /**
         * OneSignal Push Notifications Parameters
         * Variables defined in GlobalVars.ts
         * Create tag based on User defined categories
         */
       /*  this.oneSignal.startInit(this.globals.appIdOneSignal, this.globals.googleOneSignalID);
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert); 
        var parseCategories = localStorage.getItem('alerts');
        if (parseCategories != null)
        {
          var getCategories = parseCategories.split(",");
          this.oneSignal.deleteTags(getCategories);
          for(var a = 0; a < getCategories.length; a++)
          {
            if (getCategories[a] != 'null')
              this.oneSignal.sendTag(getCategories[a], "1");
          }
        }
        this.oneSignal.deleteTag('name');
        this.oneSignal.sendTag('name', localStorage.getItem('reduce-name'));
        this.oneSignal.handleNotificationOpened().subscribe(data => this.onPushOpened(data.notification.payload));
        this.oneSignal.endInit();
         */
        splashScreen.hide();
      });
    } 
  }
  

  /**
   * Private method for OneSignal Open Push Notifications
   * @param payload 
   */
  private onPushOpened(payload: OSNotificationPayload) 
  {
    var store = payload.additionalData;
    var buttons = payload.actionButtons[0];
    if (buttons.id == "gotostore")
    {
      let options: LaunchNavigatorOptions = {
        appSelection: {
          dialogHeaderText: "Navegar até o local usando...",
          cancelButtonText: "Cancelar",
          rememberChoice: {
            enabled: false,
            prompt: {
              headerText: "Navegar até o local",
              bodyText: "Utilizar este App na próxima vez?",
              yesButtonText: "SIM",
              noButtonText: "NÃO"
            }
          }
      }};
      this.launchNavigator.navigate(store.address, options)
        .then(
          success => console.log('Launched navigator'),
          error => console.log('Error launching navigator', error)
        );
    }
  }


  /**
   * Method to show Toast messages 
   * on bottom of device
   * @param msgToast 
   */
  presentToast(msgToast) 
  {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }


  /**
   * Method to show Home Page Globally from Cart 
   * on bottom of device
   */
  openHomePage(){
    this.globals.closeCart();
    if (this.globals.isHomePage == false)
    {
      let nav = this.app.getRootNav();
      nav.setRoot(HomePage);
    }    
  }


  /**
   * Method to show Payment page Globally from Cart 
   * on bottom of device
   */
  openPayment(){
    let nav = this.app.getRootNav();
    this.globals.closeCart();
    var val = localStorage.getItem('logged');
    if (val === 'facebook' || val === 'email'){
      nav.push(PagamentoPage);
    }else{
      nav.setRoot(LoginPage, {shopInProgress: true});
    }
  }

  refreshNetwork(){
    this.network.onConnect().subscribe(() => {
      if (this.globals.networkDown)
      {
        // hide NotConnection View
        this.presentToast('Opa, você está online novamente!');
        this.globals.networkDown = false;
      }
    });
  }

} 