import { NgModule, ErrorHandler, LOCALE_ID, enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule } from 'ionic-angular';
import { CacheModule } from "ionic-cache-observable";
import { MyApp } from './app.component';
import { Geolocation } from '@ionic-native/geolocation'; 
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { registerLocaleData, DatePipe } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 

import { TermosPage } from '../pages/termos/termos';
import { HomePage } from '../pages/home/home';
import { LojasPage } from '../pages/lojas/lojas';
import { LoginPage } from '../pages/login/login';
import { StoresProvider } from '../providers/stores';
import { Facebook } from '@ionic-native/facebook';

import { TokenInterceptor } from '../interceptors/token.interceptor';
import { AppErrorHandler } from './app.error-handler';
import { GlobalVars } from '../providers/globalVars';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
import { AuthProvider } from '../providers/auth';
import { CategoriesPage } from '../pages/categorias/categorias';
import { CategoriesProvider } from '../providers/categories';
import { LojaPage } from '../pages/lojas/loja';
import { SocialSharing } from '@ionic-native/social-sharing';
import { LocalNotifications } from '@ionic-native/local-notifications';
//import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { TermsProvider } from '../providers/terms';
import { MaskmoneyProvider } from '../providers/maskmoney';
import { MapPage } from '../pages/map/map';
import { GeolocationsProvider } from '../providers/geolocations';
import { SenhaPage } from '../pages/senha/senha';
import { OneSignal } from '@ionic-native/onesignal';
import { ProductsProvider } from '../providers/products';
import { TokenProvider } from '../providers/token';
import { PlansProvider } from '../providers/plans';
import { CadastroPage } from '../pages/cadastro/cadastro';
import { SignupProvider } from '../providers/signup';
import { AddressProvider } from '../providers/address'
import { PagamentoPage } from '../pages/pagamento/pagamento';
import { EnderecosPage } from '../pages/enderecos/enderecos';
import { AlertasPage } from '../pages/alertas/alertas';
import { IndicalojaPage } from '../pages/indicacoes/loja';
import { SearchProvider } from '../providers/search';
import { PaymentProvider } from '../providers/payment';
import { TextMaskModule } from 'angular2-text-mask';
import { OfferProvider } from '../providers/offer';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { DivulgaPage } from '../pages/divulga/divulga';
import { DestaquesPage } from '../pages/destaques/destaques';
import { IndicationProvider } from '../providers/indications';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { Alerts } from '../providers/alerts';
import { VerofertasPage } from '../pages/verofertas/verofertas';
import { VerprodutoPage } from '../pages/verproduto/verproduto';
import { FotoPage } from '../pages/verproduto/foto';
import { AlteracaoPage } from '../pages/cadastro/alteracao';
import { UtilsProvider } from '../providers/utils';
import { SorteiosPage } from '../pages/sorteios/sorteios';
import { LotteryProvider } from '../providers/lottery';

import { AngularCropperjsModule } from 'angular-cropperjs';
import { Camera } from '@ionic-native/camera';
import { FavoritosPage } from '../pages/favoritos/favoritos';
import { CacheService } from './cache.service';
import { CartProvider } from '../providers/cart';
import { Clipboard } from '@ionic-native/clipboard';
import { PedidosPage } from '../pages/pedidos/pedidos';
import { CarteiraPage } from '../pages/carteira/carteira';
import { SeligapayPage } from '../pages/pagamento/seligapay';
import { ContratoPage } from '../pages/carteira/contrato';

registerLocaleData(localePt);

@NgModule({
  declarations: [
    MyApp,
    CarteiraPage,
    ContratoPage,
    TermosPage,
    FotoPage,
    HomePage,
    PedidosPage,
    AlteracaoPage,
    SorteiosPage,
    FavoritosPage,
    DestaquesPage,
    SenhaPage,
    VerprodutoPage,
    DivulgaPage,
    CadastroPage,
    VerofertasPage,
    PagamentoPage,
    SeligapayPage,
    EnderecosPage,
    AlertasPage,
    LoginPage,
    MapPage,
    LojasPage,
    LojaPage,
    IndicalojaPage,
    CategoriesPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AngularCropperjsModule,
    BrowserAnimationsModule,
    BrMaskerModule,
    TextMaskModule,
    IonicModule.forRoot(MyApp, {
        backButtonText: ''
     }),
    IonicStorageModule.forRoot({
      name: '__sladb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    CacheModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TermosPage,
    CarteiraPage,
    ContratoPage,
    HomePage,
    AlteracaoPage,
    VerofertasPage,
    SorteiosPage,
    PedidosPage,
    FotoPage,
    FavoritosPage,
    CadastroPage,
    DivulgaPage,
    VerprodutoPage,
    DestaquesPage,
    PagamentoPage,
    SeligapayPage,
    SenhaPage,
    EnderecosPage,
    MapPage,
    AlertasPage,
    LojasPage,
    LojaPage,
    LoginPage,
    CategoriesPage,
    IndicalojaPage
  ],
  providers: [
    StatusBar,
    Network,
    SplashScreen,
    Facebook,
    Alerts,
    StoresProvider,
    Camera,
    ProductsProvider,
    CacheService,
    TermsProvider,
    OneSignal,
    MaskmoneyProvider,
    LotteryProvider,
    SearchProvider,
    LaunchNavigator,
    DatePipe,
    PlansProvider,
    UtilsProvider,
    Clipboard,
    //BarcodeScanner,
    CartProvider,
    LocalNotifications,
    IndicationProvider,
    PaymentProvider,
    SignupProvider,
    OfferProvider,
    AddressProvider,
    GeolocationsProvider,
    SocialSharing,
    CategoriesProvider,
    AuthProvider,
    TokenProvider,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: ErrorHandler, useClass: AppErrorHandler },
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    Geolocation,
    GlobalVars
  ]
})
export class AppModule {}
enableProdMode();