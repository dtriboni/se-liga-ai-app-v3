import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { CategoriesProvider } from '../../providers/categories';
import { LojasPage } from '../lojas/lojas';
import { GlobalVars } from '../../providers/globalVars';

@Component({
  selector: 'page-categorias',
  templateUrl: 'categorias.html'
})
export class CategoriesPage {

  categories = []

  constructor(private toastCtrl: ToastController, 
    private loadingCtrl: LoadingController, 
    private globals: GlobalVars,
    private categoriesProvider: CategoriesProvider,
    public navCtrl: NavController) {
      this.globals.isHomePage = false;
      this.globals.showFooterOnSomePages = false;
    }

  ionViewDidLoad() { }

  ionViewWillEnter() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Buscando categorias...'});
    loading.present();
    this.categoriesProvider.categories()
      .subscribe(data => {
        this.categories = data;
        loading.dismiss();
      },
      error => { 
        loading.dismiss();
        this.presentToast("Não foi possível obter categorias no momento.");
      });
  }

  /**
   * Method to show Toast messages 
   * on bottom of device
   * @param msgToast 
   */
  presentToast(msgToast) 
  {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  getStoresCategory(id, category){
    this.navCtrl.push(LojasPage, {catID: id, catName: category});
  }

  

}
