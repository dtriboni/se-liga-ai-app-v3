import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { SearchProvider } from '../../providers/search';
import { GlobalVars } from '../../providers/globalVars';
import { VerprodutoPage } from '../verproduto/verproduto';
import { Geolocation } from '@ionic-native/geolocation';

@Component({
  selector: 'page-destaques',
  templateUrl: 'destaques.html'
})
export class DestaquesPage {

  items = [];
  searchTerm: string = '';
  parseCategories: string = null;
  nohighlights: boolean = false;

  constructor(private toastCtrl: ToastController, 
              public navCtrl: NavController, 
              public coordinates: GlobalVars,
              public geolocation: Geolocation,
              private loadingCtrl: LoadingController, 
              public pesquisaProvider: SearchProvider) 
  {
    this.coordinates.isHomePage = false;
    this.coordinates.showFooterOnSomePages = true;
  }

  doRefresh(refresher) {
    this.ionViewWillEnter(false);
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  ionViewWillEnter(load: boolean = true){
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Buscando destaques...'});
      if (load)   
        loading.present();

    var newlat = localStorage.getItem('newlat');
    var newlng = localStorage.getItem('newlng');
    this.searchTerm = '';
    this.parseCategories = localStorage.getItem('alerts');
    var val = null;

    if (this.parseCategories != null){
      var getCategories = this.parseCategories.split(",");
      for(var a = 0; a < getCategories.length; a++){
        if (getCategories[a].trim() != 'null'){
          this.searchTerm += " c.icon = '" + getCategories[a] + "' OR ";
        }
      }
      val = { categories: this.searchTerm, latitude: newlat, longitude: newlng };
    }

    if (newlat != "0" && newlng != "0" && newlat != null && newlng != null){

      this.coordinates.latitude = parseFloat(newlat);
      this.coordinates.longitude = parseFloat(newlng);

      this.pesquisaProvider.highlights(val).subscribe(res => {
          this.nohighlights = false;
          this.items = res;
          if (load) 
            loading.dismiss();
      }, error => {
          if (load) 
            loading.dismiss();  
          this.nohighlights = true;  
          this.presentToast("Não foi possível exibir os destaques no momento.");
      });

    }else{

      const watch = this.geolocation.watchPosition().subscribe(position => {
        this.coordinates.latitude = position.coords.latitude;
        this.coordinates.longitude = position.coords.longitude;
       
        this.pesquisaProvider.highlights(val).subscribe(res => {
            this.nohighlights = false;
            this.items = res;
            if (load) 
              loading.dismiss();
            watch.unsubscribe();
        }, error => {
            if (load) 
              loading.dismiss();
            watch.unsubscribe();
            this.nohighlights = true;
            this.presentToast("Não foi possível exibir os destaques no momento.");
        });

      }, error => {
          watch.unsubscribe();
          this.nohighlights = true;
          loading.dismiss();
      });
    }   
  }

  getProductDetails(id, store){
    this.navCtrl.push(VerprodutoPage, {ofeID: id, strName: store});
  }

  getDistance(lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - this.coordinates.latitude);  // deg2rad below
    var dLon = this.deg2rad(lon2 - this.coordinates.longitude); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(this.coordinates.latitude)) * Math.cos(this.deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    var distance = d.toFixed(1);
    var metric = (d < 1 ? (parseFloat(distance) * 1000) + " m": distance + " km");
    if ((this.coordinates.latitude != 0))
      return metric.replace(".", ",");
    else
      return 'calculando...'
  }

  deg2rad(deg) {
    return deg * (Math.PI/180)
  }

  /**
   * Method to show Toast messages 
   * on bottom of device
   * @param msgToast 
   */
  presentToast(msgToast) 
  {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

}
