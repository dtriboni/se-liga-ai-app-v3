import { Component } from '@angular/core';
import { NavController, AlertController, ToastController, ModalController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, ValidatorFn, AbstractControl } from '@angular/forms';
import { LoadingController } from '../../../node_modules/ionic-angular/components/loading/loading-controller';
import { SignupProvider } from '../../providers/signup';
import { EnderecosPage } from '../enderecos/enderecos';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { UtilsProvider } from '../../providers/utils';
import { HomePage } from '../home/home';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { GlobalVars } from '../../providers/globalVars';

@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html'
})
export class CadastroPage {

    public form: FormGroup;
    imgLogo: string = ",,/../assets/imgs/logo.png";
    debouncer: any;
    submitAttempt: boolean = false;
    usrID: number = 0;
    signed: boolean = true;
    isFemale: boolean = false;
    isMale:boolean = false;
    usr_name: string
    usr_email: string;
    usr_datanasc: string;
    usr_cpfcnpj: string;
    usr_gender: string;
    currentMask: object = {mask:'00000000000000', len:14};
   
  constructor(public navCtrl: NavController,
    private loadingCtrl: LoadingController, 
    private globals: GlobalVars,
    public navParams: NavParams,
    private signupProvider: SignupProvider,
    private formBuilder: FormBuilder,
    public modalCtrl: ModalController,
    private utils: UtilsProvider,
    private toastCtrl: ToastController) {

      this.globals.isHomePage = false;
      this.globals.showFooterOnSomePages = false;
      this.usrID = this.navParams.get('usrID');

      this.usr_name = localStorage.getItem('usr-name');
      this.usr_email = localStorage.getItem('usr-email');
      this.usr_gender = localStorage.getItem('usr-gender');
      this.usr_datanasc = localStorage.getItem('usr-datanasc');
      this.usr_cpfcnpj = localStorage.getItem('usr-cpfcnpj');

      if (this.usr_gender == 'male'){
        this.isMale = true;
      }
      if (this.usr_gender == 'female'){
        this.isFemale = true;
      }
      if (this.usr_cpfcnpj == null){
        this.signed = false;
      }
     
        this.form = this.formBuilder.group({
          name: ['', Validators.compose([Validators.maxLength(50), Validators.required])],
          gender: ['', Validators.compose([Validators.required])],
          email: ['', Validators.compose([Validators.maxLength(40), Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'), Validators.required])],
          pwd: ['', Validators.compose([Validators.maxLength(8), Validators.minLength(6), Validators.pattern('[a-zA-Z0-9_ ]*'), Validators.required])],
          cpwd: ['', Validators.compose([Validators.maxLength(8), Validators.minLength(6), Validators.pattern('[a-zA-Z0-9_ ]*'), Validators.required, this.equalto('pwd')])],
          signup: 'email',
          datanasc: ['', Validators.compose([Validators.required])],
          cpfcnpj: ['', Validators.compose([this.checkCPFCNPJ("cpfcnpj")])]
        });
  }

  public storeUser: boolean = false;
  
  signUp()
  {
    this.submitAttempt = true;
    
    if(this.form.valid)
    {
      let loading = this.loadingCtrl.create({
          spinner: 'crescent',
          content: 'Por favor, aguarde...'});
      loading.present(); 
      
      if (this.usrID > 0)
      {
        this.form.value.email = (localStorage.getItem('usr-email'));
        if (this.usr_cpfcnpj != null){
          this.form.value.cpfcnpj = (localStorage.getItem('usr-cpfcnpj'));
        }
        this.signupProvider.updatesignup(this.form.value).subscribe(res => {

          loading.dismiss();
          var yourName = localStorage.getItem('reduce-name');
          this.presentToast(yourName + ", seu cadastro no Se Liga Aí foi atualizado com sucesso!");
          this.navCtrl.setRoot(HomePage);

        }, erro => {

          loading.dismiss();
          this.presentToast("Não foi possível atualizar seus dados de cadastro!");

        });

      }else{

        this.signupProvider.checkemail(this.form.value.email).subscribe(numemail => {
          
          if (numemail > 0)
          {
            loading.dismiss();
            this.presentToast("Informe um outro email para cadastro. " + this.form.value.email + " já está cadastrado!");
            this.form.controls["email"].setValue("");

          }else{

            this.signupProvider.signup(this.form.value).subscribe(res => {

              loading.dismiss();
              var yourName = localStorage.getItem('reduce-name');
              var yourGender = localStorage.getItem('gender');  
              this.presentToast("Bem vind" + (yourGender == "male" ? "o ": "a ") + yourName + "! Seu cadastro no Se Liga Aí foi efetuado com sucesso!");
              this.navCtrl.setRoot(HomePage);

            }, erro => {

              loading.dismiss();
              this.presentToast("Não foi possível salvar seus dados de cadastro!");

            });
          }

        },error => {

          loading.dismiss();
          this.presentToast("Não foi possível verificar seu email já está cadastrado!");
         
        });
      }
    }
  }

  equalto(field_name): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        let input = control.value;
        let isValid = control.root.value[field_name] == input;
        if (!isValid)
            return {'equalTo': {isValid}};
        else
            return null;
    };
  }

  checkCPFCNPJ(field_name): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      var cpfcnpj = control.root.value[field_name];
      var input = control.value;
      cpfcnpj = input;
      if (cpfcnpj != undefined){
        cpfcnpj = cpfcnpj.replace(/[^\d]+/g,'');
        if (cpfcnpj.length == 11){
          if(this.utils.validCPF(cpfcnpj)){
            this.currentMask = {mask:'000.000.000-00', len:14};
            return null;
          }else{
            return {'checkCPF' : false};
          }
        }
        if (cpfcnpj.length == 14){
          if(this.utils.validCNPJ(cpfcnpj)){
            this.currentMask = {mask:'00.000.000/0000-00', len:18};
            return null;
          }else{
            return {'checkCNPJ' : false};
          }
        }
        if (cpfcnpj.length != 11 && cpfcnpj.length != 14){
            this.currentMask = {mask:'00000000000000', len:14};
            return {'checkCPFCNPJ' : false};
        }
      }else{
        this.currentMask = {mask:'00000000000000', len:14};
        return null;
      }
    }
  }

  presentToast(msgToast) {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

}
