import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { AddressProvider } from '../../providers/address';
import { EnderecosPage } from '../enderecos/enderecos';
import { CadastroPage } from './cadastro';
import { OfferProvider } from '../../providers/offer';
import { GlobalVars } from '../../providers/globalVars';
import { CarteiraPage } from '../carteira/carteira';

@Component({
  selector: 'page-alteracao',
  templateUrl: 'alteracao.html'
})
export class AlteracaoPage {

    address = [];
    signup = {};
    offers = [];

  constructor(public navCtrl: NavController, 
            private toastCtrl: ToastController, 
            private loadingCtrl: LoadingController,
            private globals: GlobalVars,
            private addressProvider: AddressProvider) {

            this.globals.showFooterOnSomePages = false;
            this.globals.isHomePage = false;
  }

  ionViewWillEnter(){
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Buscando seus dados...'});
    loading.present();
    this.addressProvider.getaddresses()
      .subscribe(data => {
        this.address = data;
        this.signup = this.address[0];
        loading.dismiss();
      },
      error => { 
        loading.dismiss();
        this.presentToast("Não foi possível obter seus dados pessoais. Tente novamente mais tarde!");
      });
  } 

  newAddress(){
    this.navCtrl.push(EnderecosPage, {});
  }

  updateAddress(uadid){
    this.navCtrl.push(EnderecosPage, {addID: uadid});
  }

  updateSign(usrid){
    this.navCtrl.push(CadastroPage, {usrID: usrid});
  }

  removeAddress(uadid){
    let loading = this.loadingCtrl.create({
        spinner: 'crescent',
        content: 'Excluindo endereço...'});
      loading.present();
      this.addressProvider.removeaddress(uadid)
        .subscribe(data => {
          loading.dismiss();
          this.ionViewWillEnter();
        },
        error => { 
          loading.dismiss();
          this.presentToast("Não foi possível excluir este endereço. Tente novamente mais tarde!");

        });   
  }

  /**
   * Method to show Toast messages 
   * on bottom of device
   * @param msgToast 
   */
  presentToast(msgToast) 
  {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  openWallet(){
    this.navCtrl.push(CarteiraPage, {});
  }

}
