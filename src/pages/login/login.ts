import { Component } from '@angular/core';
import { Facebook } from '@ionic-native/facebook';
import { NavController, LoadingController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AuthProvider } from '../../providers/auth';
import { ToastController } from 'ionic-angular';
import { CadastroPage } from '../cadastro/cadastro';
import { SignupProvider } from '../../providers/signup';
import { SenhaPage } from '../senha/senha';
import { HomePage } from '../home/home';
import { GlobalVars } from '../../providers/globalVars';
import { PagamentoPage } from '../pagamento/pagamento';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {

    isLoggedIn: boolean
    public form: FormGroup;
    public shopInProgress: boolean = false;

    constructor(private navCtrl: NavController, 
                public navParams: NavParams,
                private formBuilder: FormBuilder,
                private authProvider: AuthProvider,
                private globals: GlobalVars,
                private signupProvider: SignupProvider,
                private loadingCtrl: LoadingController,
                public facebook: Facebook,
                private toastCtrl: ToastController) 
    {
        this.shopInProgress = this.navParams.get('shopInProgress');

        this.form = this.formBuilder.group({
            'user': ['', Validators.compose([Validators.required])],
            'pwd': ['', Validators.compose([Validators.required])]
        });

        this.globals.isHomePage = false;
        this.globals.showFooterOnSomePages = false;
    }

    ionViewCanEnter(): boolean {
        var val = localStorage.getItem('logged')
        if (val === 'facebook' || val === 'email'){
            this.navCtrl.setRoot(HomePage);
        }
        return true;
    }

    loginEmail() 
    {
        let loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: 'Efetuando login...'});
          loading.present();
          this.authProvider.login(this.form.value).subscribe(() => {
                if (this.shopInProgress == true){
                    this.navCtrl.push(PagamentoPage);
                }else{
                    this.navCtrl.setRoot(HomePage);
                }
              loading.dismiss();
            }, error => {
              loading.dismiss();
              this.presentToast("Informe seu Email e sua Senha corretamente!");
            });
    }

     loginFacebook() {
        let loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: 'Efetuando login...'});
          loading.present();
        let permissions = new Array<string>();
        permissions = ["public_profile", "email"];
        this.facebook.getLoginStatus()
        .then(res => {
            if(res.status === "connected") {
                localStorage.setItem('logged', 'facebook');
                this.getUserDetail(res.authResponse.userID, loading);
            } else {
                localStorage.setItem('logged', 'false');
                this.facebook.login(permissions).then((response) => {
                    let params = new Array<string>();
                    this.facebook.api("/me?fields=name,email", params)
                    .then(res => {
                        let usuario = new Usuario();
                        usuario.nome = res.name;
                        usuario.email = res.email;
                        usuario.senha = res.id;
                        usuario.login = res.email;
                        usuario.codigo = res.id;
                        this.logar(usuario, loading);
                    }, (error) => {
                        loading.dismiss();
                        this.presentToast("Não foi possível entrar com Facebook. Tente novamente mais tarde.");
                    })
                }, (error) => {
                    loading.dismiss();
                    this.presentToast("Não foi possível conectar-se com o Facebook. Tente logar-se via Email e Senha cadastrados no Se Liga Aí!");
                });
            }
        })
        .catch(e => {});
    }

    logar(usuario: Usuario, loading) {
        localStorage.setItem('logged', 'facebook');
        this.getUserDetail(usuario.codigo, loading);
    }

    logout() {
        localStorage.clear();
        this.facebook.logout()
        .then( res => this.isLoggedIn = false)
        .catch(e => console.log('Error logout from Facebook', e));
    }

    signup() {
        //this.navCtrl.setRoot(CadastroPage, {usrID: 0});
        this.navCtrl.push(CadastroPage, {});
    }

    renew() {
        //this.navCtrl.setRoot(SenhaPage);
        this.navCtrl.push(SenhaPage, {});
    }

    getUserDetail(userid, loading) {
        // fazer redirect 
        this.facebook.api("/"+userid+"/?fields=id,email,name,picture,gender",["public_profile"])
            .then(res => {

                this.signupProvider.checkemail(res.email)
                .subscribe(numemail => {

                  if (numemail > 0){ 

                    this.form.value.user = res.email;
                    this.form.value.pwd = 'seligaai';
                    this.authProvider.login(this.form.value).subscribe(() => {
                        localStorage.removeItem('usr-picture');
                        localStorage.setItem('usr-picture', res.picture.data.url);
                        if (this.shopInProgress == true){
                            this.navCtrl.push(PagamentoPage);
                        }else{
                            this.navCtrl.setRoot(HomePage);
                        }
                        loading.dismiss();
                    }, err => {
                        loading.dismiss();
                        localStorage.setItem('logged', 'false');
                        this.presentToast("O email " + res.email + " já está cadastrado no Se Liga Aí!");
                    });

                  }else{ 

                    this.signupProvider.signup({name: res.name, 
                                                gender: 'male', 
                                                signup: 'facebook', 
                                                datanasc: '1980-01-01', 
                                                cpfcnpj: '', 
                                                email: res.email, 
                                                pwd: 'seligaai'})
                    .subscribe(ret => {
                        localStorage.setItem('usr-name', res.name);
                        localStorage.setItem('usr-type', '1');
                        localStorage.setItem('logged', 'facebook');
                        localStorage.setItem('usr-email', res.email);
                        localStorage.setItem('usr-picture', res.picture.data.url);
                        localStorage.setItem('reduce-name', res.name.split(" ", 1));
                        localStorage.setItem('login-info', JSON.stringify(ret));
                        localStorage.setItem('gender', res.gender);
                        this.navCtrl.setRoot(HomePage);
                        loading.dismiss();
                    }, error => {
                        loading.dismiss();
                        this.presentToast("Não foi possível salvar seus dados de cadastro!");
                    });           

                  }

                }, error => {

                });  


        })
        .catch(e => {
        });
    }

    showPassword(input: any, icon: any): any {
       // alert('teste');
       // input.type = input.type === 'password' ?  'text' : 'password';
       // icon.name = icon.name === 'eye-off' ? 'eye' : 'eye-off';
    }

    followTW(){
        location.href="https://twitter.com/em2dgroup";
    }

    followFB(){
        location.href="https://www.facebook.com/SELIGAAIAPP/";
    }

    followIG(){
        location.href="https://www.instagram.com/seligaaiapp/";
    }

    presentToast(msgToast) {
        let toast = this.toastCtrl.create({
          message: msgToast,
          duration: 5000,
          position: 'bottom'
        });
      
        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });
      
        toast.present();
      }

}

export class Model {
    constructor(objeto?) {
        Object.assign(this, objeto);
    }
}
  
export class Usuario extends Model {
    codigo: number;
    nome: string;
    email: string;
    login: string;
    senha: string;
}