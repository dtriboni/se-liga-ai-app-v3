import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { StoresProvider } from '../../providers/stores';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Geolocation } from '@ionic-native/geolocation';
import { GlobalVars } from '../../providers/globalVars';
import { LojaPage } from './loja';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-lojas',
  templateUrl: 'lojas.html',
})
export class LojasPage {

  stores = [];
  data: any;
  nostores: boolean = false;
  page = 1;
  perPage = 0;
  totalData = 0;
  totalPage = 0;
  catID: any;
  catName: string;
  isSellers: boolean = false;
  startPosition: any;
  coordinate;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private toastCtrl: ToastController,
    private coordinates: GlobalVars,
    private storesProvider: StoresProvider,
    private geolocation: Geolocation,
    private loadingCtrl: LoadingController
  ) {
    this.catID = this.navParams.get('catID');
    this.catName = this.navParams.get('catName');
    this.isSellers = this.navParams.get('isSellers');

    this.coordinates.isHomePage = false;
    this.coordinates.showFooterOnSomePages = true;

    this.coordinate = {
      latitude: '', 
      longitude: ''
    }
  }
  
  doInfinite(infiniteScroll) {
    this.page = this.page+1;
    setTimeout(() => {
      if (!this.isSellers)
      {
        this.storesProvider.stores(this.coordinate, this.page, this.catID).subscribe(
          res => {
            this.data = res;
            this.perPage = this.data.per_page;
            this.totalData = this.data.total;
            this.totalPage = this.data.total_pages;
            for(let i=0; i<this.data.stores.length; i++) {
              this.stores.push(this.data.stores[i]);
            }
          },
          error => {});
      }
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 1000);
  }

  ionViewWillEnter() {

    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Buscando ' + this.catName + '...'});
    loading.present();
    this.page = 1;
    this.totalPage = 0;

    var newlat = localStorage.getItem('newlat');
    var newlng = localStorage.getItem('newlng');

    if (newlat != "0" && newlng != "0" && newlat != null && newlng != null){

        this.coordinates.latitude = parseFloat(newlat);
        this.coordinates.longitude = parseFloat(newlng);
        this.coordinate.latitude = newlat;
        this.coordinate.longitude = newlng;

        if (this.isSellers)
        {
          this.storesProvider.sellers(this.coordinate, -1)
          .subscribe(data => {
            this.stores = data.sellers;
            this.nostores = false;
            loading.dismiss();
          },
          error => { 
            loading.dismiss();
            this.nostores = true;
            this.presentToast("Nenhum ítem de " + this.catName + " encontrado.");
          });
        }else{
          this.storesProvider.stores(this.coordinate, this.page, this.catID)
          .subscribe(data => {
            this.stores = data.stores;
            this.data = data;
            this.perPage = this.data.per_page;
            this.totalData = this.data.total;
            this.totalPage = this.data.total_pages;
            this.nostores = false;
            loading.dismiss();
          },
          error => { 
            loading.dismiss();
            this.nostores = true;
            this.presentToast("Nenhum ítem de " + this.catName + " encontrado.");
          });
        }


    }else{

      const watch = this.geolocation.watchPosition().subscribe(position => {
        this.coordinates.latitude = position.coords.latitude;
        this.coordinates.longitude = position.coords.longitude;
        this.coordinate.latitude = position.coords.latitude;
        this.coordinate.longitude = position.coords.longitude;  

        if (this.isSellers)
        {
          this.storesProvider.sellers(this.coordinate, -1)
          .subscribe(data => {
            this.stores = data.sellers;
            this.nostores = false;
            loading.dismiss();
            watch.unsubscribe();
          },
          error => { 
            watch.unsubscribe();
            this.nostores = true;
            loading.dismiss();
            this.presentToast("Nenhum ítem de " + this.catName + " encontrado.");

          });
        }else{

          this.storesProvider.stores(this.coordinate, this.page, this.catID)
          .subscribe(data => {
            this.stores = data.stores;
            this.data = data;
            this.perPage = this.data.per_page;
            this.totalData = this.data.total;
            this.totalPage = this.data.total_pages;
            this.nostores = false;
            loading.dismiss();
            watch.unsubscribe();
          },
          error => { 
            watch.unsubscribe();
            this.nostores = true;
            loading.dismiss();
            this.presentToast("Nenhum ítem de " + this.catName + " encontrado.");

          });
        }
  
         
      }, error => {
          watch.unsubscribe();
          loading.dismiss();
          this.navCtrl.setRoot(HomePage);
      });
      
    }
  }

  presentToast(msgToast) {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 5000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  getDistance(lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - this.coordinates.latitude);  // deg2rad below
    var dLon = this.deg2rad(lon2 - this.coordinates.longitude); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(this.coordinates.latitude)) * Math.cos(this.deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    var distance = d.toFixed(1);
    var metric = (d < 1 ? (parseFloat(distance) * 1000) + " m": distance + " km");
    if ((this.coordinates.latitude != 0))
      return metric.replace(".", ",");
    else
      return 'calculando...'
  }

  deg2rad(deg) {
    return deg * (Math.PI/180)
  }

  getStoreDetails(id, store){
    this.navCtrl.push(LojaPage, {strID: id, strName: store});
  }

}
