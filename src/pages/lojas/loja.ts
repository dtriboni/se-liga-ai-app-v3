import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { StoresProvider } from '../../providers/stores';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Geolocation } from '@ionic-native/geolocation';
import { GlobalVars } from '../../providers/globalVars';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { VerofertasPage } from '../verofertas/verofertas';
import { MapPage } from '../map/map';
import { CartProvider } from '../../providers/cart';
import { Clipboard } from '@ionic-native/clipboard';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'page-loja',
  templateUrl: 'loja.html',
})
export class LojaPage {

  stores = [];
  bt_show_offer: string = "VER OFERTA";
  msg_discount: string = "com desconto";
  strID: any;
  strName: string;
  startPosition: any;
  storeAddress: any;
  isFav:boolean = false;
  fav:string = 'FAVORITAR';
  retFavourites: string = null;
  open_time: boolean = false;
  parseFavourites: string = null;
  txt_description_all: any;
  txt_description_short: any;
  show_description: boolean = false;
  num_descr: number = 0;
  coupons = [];
  youtubeURL;
  noitemcoupon: boolean = false;
  loadingcoupon: boolean = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private clipboard: Clipboard,
    private _sanitizer: DomSanitizer,
    private coordinates: GlobalVars,
    private storesProvider: StoresProvider,
    private cartProvider: CartProvider,
    private toastCtrl: ToastController,
    private geolocation: Geolocation,
    private socialSharing: SocialSharing,
    private loadingCtrl: LoadingController,
    private launchNavigator: LaunchNavigator
  ) {
    this.strID = this.navParams.get('strID');

    this.coordinates.isHomePage = false;
    this.coordinates.showFooterOnSomePages = true;

    this.parseFavourites = localStorage.getItem('favourites');
    if (this.parseFavourites != null){
        this.retFavourites = this.parseFavourites;
        var getFavourites = localStorage.getItem('favourites').split(",");
        for(var a = 0; a < getFavourites.length; a++){
          if (getFavourites[a] != null){
            if (getFavourites[a] == this.strID){
              this.isFav = true;
              this.fav = 'FAVORITO';
              break;
            }
          }
        }
    }
  }

  openTime(){
    this.open_time = true;
  }

  cleanURL(oldURL: string): SafeResourceUrl {
    return this._sanitizer.bypassSecurityTrustResourceUrl(oldURL);
  }

  openDescription(){
    this.txt_description_short = this.txt_description_all;
    this.show_description = false;
  }

  getCurrentPosition(){
    var newlat = localStorage.getItem('newlat');
    var newlng = localStorage.getItem('newlng');
    if (newlat != "0" && newlng != "0" && newlat != null && newlng != null){
        this.coordinates.latitude = parseFloat(newlat);
        this.coordinates.longitude = parseFloat(newlng);
    }else{
      const watch = this.geolocation.watchPosition().subscribe(position => {
        this.coordinates.latitude = position.coords.latitude;
        this.coordinates.longitude = position.coords.longitude;
      });
      watch.unsubscribe();
    }
  }

  ionViewDidLoad() { 
    this.getCurrentPosition(); 
  }
  
  stopAllYouTubeVideos() { 
    var iframes = document.querySelectorAll('iframe');
    Array.prototype.forEach.call(iframes, iframe => { 
      iframe.contentWindow.postMessage(JSON.stringify({ event: 'command', 
    func: 'stopVideo' }), '*');
   });
  }

  ionViewWillEnter() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Por favor, aguarde...'});
    loading.present();
    
    this.storesProvider.store( this.strID )
      .subscribe(data => {
        this.stores = data;
        this.strName = data[0].store;
        this.num_descr = data[0].num_descr;
        if (this.num_descr > 150)
        {
          this.txt_description_short = data[0].description.substr(0, 150) + " ...";
          this.txt_description_all = data[0].description;
          this.show_description = true;
        }else{
          this.txt_description_all = data[0].description;
          this.txt_description_short = this.txt_description_all;
          this.show_description = false;
        }
        this.bt_show_offer = (data[0].offers_count > 1 ? "VER OFERTAS" : "VER OFERTA");
        this.msg_discount = (data[0].offers_count > 1 ? "com descontos" : "com desconto");
        this.storeAddress = data[0].address + ", " + data[0].number + " - CEP " + data[0].zipcode + " - " + data[0].neighboor + ", " + data[0].city +"/" + data[0].state;
        var yt = data[0].youtube;
        this.youtubeURL = this.cleanURL(yt.replace("/watch?v=", "/embed/"));
        this.cartProvider.coupons( data[0].strid ).subscribe(data => 
        {
          this.coupons = data.coupons;
          this.noitemcoupon = false;
          this.loadingcoupon = false;                  
        },error => {
          this.noitemcoupon = true;
          this.loadingcoupon = false;    
        }); 
        
        loading.dismiss();
      },
      error => { 
        loading.dismiss();
        this.presentToast("Não foi possível obter os dados do local.");
      });
  }

  showOffers(id, store){
    this.navCtrl.push(VerofertasPage, {addID: id, strName: store});
  }

  copyText(txt, store){
    //$ ionic cordova plugin add cordova-clipboard
    //$ npm install --save @ionic-native/clipboard@3
    this.clipboard.copy(txt);
    var local = "Utilize-o em sua compra no Se Liga Aí.";
    this.presentToast("Cupom " + txt + " copiado! " + local);
  }

  goToStore(){
    let options: LaunchNavigatorOptions = {
      appSelection: {
        dialogHeaderText: "Navegar até o local usando...",
        cancelButtonText: "Cancelar",
        rememberChoice: {
          enabled: false,
          prompt: {
            headerText: "Navegar até o local",
            bodyText: "Utilizar este App na próxima vez?",
            yesButtonText: "SIM",
            noButtonText: "NÃO"
          }
        }
    }};
    this.launchNavigator.navigate(this.storeAddress, options)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }

  showMap(lat, lng, name, add, offe, icon) {
    this.navCtrl.push(MapPage, {cLat: lat, cLng: lng, cName: name, cAdd: add, cOffe: offe, cIcon: icon});
  }

  share(store, image) {
    this.socialSharing.share('Se liga! ' + store + ' está com descontos legais, confira!', '', image, 'https://em2d.com.br/#seligaai')
  }

  callEmail(email){
    location.href="mailto:" + email;
  }

  callUrl(url){
    if (url.substring(0,7) == 'http://' || url.substring(0,8) == 'https://'){
      location.href=url;
    }else{
      location.href='http://' + url;
    }
  }

  callInstagram(url){
    location.href='https://www.instagram.com/' + url;
  }

  callTwitter(url){
    location.href='https://www.twitter.com/' + url;
  }

  callTel(tel){
    location.href="tel://" + tel.replace(/\D/g, "");
  }

  callWhats(num){
    location.href="https://api.whatsapp.com/send?phone=+55" + num.replace(/\D/g, "");
  }

  shareInsta(store, image) {
    this.socialSharing.shareViaInstagram('Olha que legal, ' + store + ' está com descontos legais, confira!', image)
  }

  getDistance(lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - this.coordinates.latitude);  // deg2rad below
    var dLon = this.deg2rad(lon2 - this.coordinates.longitude); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(this.coordinates.latitude)) * Math.cos(this.deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    var distance = d.toFixed(1);
    var metric = (d < 1 ? (parseFloat(distance) * 1000) + " m": distance + " km");
    if ((this.coordinates.latitude != 0))
      return metric.replace(".", ",");
    else
      return 'calculando...'
  }

  deg2rad(deg) {
    return deg * (Math.PI/180)
  }

  favourite() {
    if (this.isFav == false){
      this.isFav = true;
      this.fav = 'FAVORITO';
      this.retFavourites += ',' + this.strID;
      setTimeout(() => {
        this.presentToast('Você favoritou ' + this.strName);
      }, 500);
    }else{
      this.isFav = false;
      this.fav = 'FAVORITAR';
      if (this.parseFavourites != null){
        var getFavourites = localStorage.getItem('favourites');
        this.retFavourites = getFavourites.replace(',' + this.strID, '');
      }
      setTimeout(() => {
        this.presentToast('Você desfavoritou ' + this.strName);
      }, 500);
    }
    localStorage.removeItem('favourites');
    localStorage.setItem('favourites', this.retFavourites+'');
  }

  presentToast(msgToast) {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 3000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }

}
