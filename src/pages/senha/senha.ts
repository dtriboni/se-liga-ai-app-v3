import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { AuthProvider } from '../../providers/auth';
import { GlobalVars } from '../../providers/globalVars';

@Component({
  selector: 'page-senha',
  templateUrl: 'senha.html'
})
export class SenhaPage {

  public form: FormGroup;
  public form2: FormGroup;
  public checked: boolean = false;
  private retrievedCode: number = 0;
  private user2: string;
  submitAttempt: boolean = false;
  submitAttempt2: boolean = false;

  constructor(public navCtrl: NavController, 
              private formBuilder: FormBuilder,
              private authProvider: AuthProvider,
              private globals: GlobalVars,
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController) {
    this.form2 = this.formBuilder.group({
      user2: ['', Validators.compose([Validators.required])],   
    });
    this.form = this.formBuilder.group({
      user: ['', Validators.compose([Validators.required])],
      code: ['', Validators.compose([Validators.maxLength(6), Validators.minLength(6), Validators.required])],
      pwd: ['', Validators.compose([Validators.maxLength(8), Validators.minLength(6), Validators.pattern('[a-zA-Z0-9_ ]*'), Validators.required])],
      cpwd: ['', Validators.compose([Validators.maxLength(8), Validators.minLength(6), Validators.pattern('[a-zA-Z0-9_ ]*'), Validators.required, this.equalto('pwd')])],    
    });
    this.globals.isHomePage = false;
    this.globals.showFooterOnSomePages = false;
  }

  rememberPassword(){
    this.submitAttempt2 = true;
    if(this.form2.valid){
      let loading = this.loadingCtrl.create({
        spinner: 'crescent',
        content: 'Por favor, aguarde...'});
      loading.present();
      this.authProvider.remember(this.form2.value.user2).subscribe(code => {
          this.checked = true;
          this.retrievedCode = code;
          this.user2 = this.form2.value.user2;
          this.presentToast("Um Email foi enviado com o código para alteração de sua senha.");
          loading.dismiss();
      }, error => {
          loading.dismiss();
          this.presentToast("Email não localizado ou preenchido incorretamente. Você realmente se cadastrou no Se Liga Aí?");
      });
    }
  }

  savePassword(){
    this.submitAttempt = true;
    if(this.form.valid){
      if (this.form.value.code == this.retrievedCode){
        let loading = this.loadingCtrl.create({
          spinner: 'crescent',
          content: 'Por favor, aguarde...'});
          loading.present();
          this.authProvider.savepassword(this.form.value).subscribe(data => {
            this.presentToast("Senha alterada. Efetue login novamente para poder entrar no Se Liga Aí!");
            loading.dismiss();
          }, error => {
            this.presentToast("Não foi possível alterar sua senha.");
            loading.dismiss();
        });
      }else{
        this.presentToast("O código que você inseriu está incorreto. Verifique o código que você recebeu no seu Email e tente novamente.");
      }
    }  
  }

  goBack(){
    this.navCtrl.setRoot(LoginPage);
  }

  equalto(field_name): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        let input = control.value;
        let isValid = control.root.value[field_name] == input;
        if (!isValid)
            return {'equalTo': {isValid}};
        else
            return null;
    };
  }

  presentToast(msgToast) {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 6000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

}
