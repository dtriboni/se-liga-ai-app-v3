import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { HomePage } from '../home/home';
import { IndicationProvider } from '../../providers/indications';
import { GlobalVars } from '../../providers/globalVars';

@Component({
  selector: 'page-indicaloja',
  templateUrl: 'loja.html'
})
export class IndicalojaPage {

    public form: FormGroup;
    submitAttempt: boolean = false;

  constructor(public navCtrl: NavController,  
              private toastCtrl: ToastController,
              private globals: GlobalVars,
              private loadingCtrl: LoadingController,
              private indicacaoProvider: IndicationProvider, 
              private formBuilder: FormBuilder) {

    this.globals.isHomePage = false;
    this.globals.showFooterOnSomePages = true;
    this.form = this.formBuilder.group({
        name: ['', Validators.compose([Validators.maxLength(50), Validators.required])],
        tel: ['', Validators.compose([Validators.maxLength(14), Validators.required])],    
      });
  }

  sendIndication() {
    this.submitAttempt = true;
    if(this.form.valid){
      let loading = this.loadingCtrl.create({
        spinner: 'crescent',
        content: 'Indicando local...'});
      loading.present();
      this.indicacaoProvider.indication(this.form.value)
      .subscribe(res => {
        loading.dismiss();
        this.presentToast("Sua indicação foi enviada com sucesso!");
      }, erro => {
        loading.dismiss();
        this.presentToast("Não foi possível enviar sua indicação!");
      });
    }
  }

  goBack() {
    this.navCtrl.setRoot(HomePage);
  }

  presentToast(msgToast) {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 5000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

}
