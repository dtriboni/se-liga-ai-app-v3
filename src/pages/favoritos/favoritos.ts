import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { StoresProvider } from '../../providers/stores';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Geolocation } from '@ionic-native/geolocation';
import { GlobalVars } from '../../providers/globalVars';
import { LojaPage } from '../lojas/loja';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-favoritos',
  templateUrl: 'favoritos.html',
})
export class FavoritosPage {

  stores = [];
  coordinate;
  startPosition: any;
  favourites: any;
  isfavourites: boolean = false;
  nofavourites: boolean = false;
  favs: string = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private toastCtrl: ToastController,
    private coordinates: GlobalVars,
    private storesProvider: StoresProvider,
    private geolocation: Geolocation,
    private loadingCtrl: LoadingController
  ) {

    this.coordinates.isHomePage = false;
    this.coordinates.showFooterOnSomePages = true;
    
    this.coordinate = {
      latitude: '', 
      longitude: '',
      stores: ''
    }
  }

 /*  doRefresh(refresher) {
    this.ionViewWillEnter();
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  } */

  ionViewWillEnter() {

    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Buscando Favoritos...'});
    loading.present();

    var newlat = localStorage.getItem('newlat');
    var newlng = localStorage.getItem('newlng');

    this.favs = 'null,';
    var checkFavourites = localStorage.getItem('favourites');
    if (checkFavourites != null){
      var getFavourites = checkFavourites.split(",");
      for(var a = 0; a < getFavourites.length; a++){
        if (getFavourites[a] != 'null' && getFavourites[a] != 'undefined' && getFavourites[a] != '' &&
        getFavourites[a] != null && getFavourites[a] != undefined && getFavourites[a].substring(0, 4) != 'null'){
          this.favs += getFavourites[a] + ',';
        }
      }
    }else{
      this.favs = '0';
    } 

    this.favourites = this.favs;    

    if (newlat != "0" && newlng != "0" && newlat != null && newlng != null){

        this.coordinates.latitude = parseFloat(newlat);
        this.coordinates.longitude = parseFloat(newlng);
        this.coordinate.latitude = newlat;
        this.coordinate.longitude = newlng;
        this.coordinate.stores = this.favourites;

        this.storesProvider.favourites(this.coordinate)
        .subscribe(data => {
          this.stores = data;
          this.isfavourites = true;
          this.nofavourites = false;
          loading.dismiss();
        },
        error => { 
          loading.dismiss();
          this.nofavourites = true;
          this.isfavourites = false;
          this.presentToast("Nenhum local favorito encontrado!");
        });

    }else{

      const watch = this.geolocation.watchPosition().subscribe(position => {
        this.coordinates.latitude = position.coords.latitude;
        this.coordinates.longitude = position.coords.longitude;
        this.coordinate.latitude = position.coords.latitude;
        this.coordinate.longitude = position.coords.longitude;  
        this.coordinate.stores = this.favourites;

          this.storesProvider.favourites(this.coordinate)
            .subscribe(data => {
              this.stores = data;
              this.nofavourites = false;
              this.isfavourites = true;
              loading.dismiss();
              watch.unsubscribe();
            },
            error => { 
              watch.unsubscribe();
              this.nofavourites = true;
              this.isfavourites = false;
              loading.dismiss();
              this.presentToast("Nenhum local favorito encontrado!");
            });
      }, error => {
          watch.unsubscribe();
          this.nofavourites = true;
          this.isfavourites = false;
          loading.dismiss();
          this.presentToast("Nenhum local favorito encontrado!");
      });
      
    }
  }

  /**
   * Method to show Toast messages 
   * on bottom of device
   * @param msgToast 
   */
  presentToast(msgToast) 
  {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  getDistance(lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - this.coordinates.latitude);  // deg2rad below
    var dLon = this.deg2rad(lon2 - this.coordinates.longitude); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(this.coordinates.latitude)) * Math.cos(this.deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    var distance = d.toFixed(1);
    var metric = (d < 1 ? (parseFloat(distance) * 1000) + " m": distance + " km");
    if ((this.coordinates.latitude != 0))
      return metric.replace(".", ",");
    else
      return 'calculando...'
  }

  deg2rad(deg) {
    return deg * (Math.PI/180)
  }

  getStoreDetails(id, store){
    this.navCtrl.push(LojaPage, {strID: id, strName: store});
  }

}
