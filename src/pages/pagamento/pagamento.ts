import { Component, ViewChild } from '@angular/core';
import { NavController, Platform, NavParams, AlertController, Content, ToastController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, AbstractControl, ValidatorFn } from '@angular/forms';
import { PaymentProvider } from '../../providers/payment';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { UtilsProvider } from '../../providers/utils';
import { GlobalVars } from '../../providers/globalVars';
import { CartProvider } from '../../providers/cart';
import { Storage } from '@ionic/storage';
import { CarteiraPage } from '../carteira/carteira';
import { SeligapayPage } from './seligapay';

@Component({
  selector: 'page-pagamento',
  templateUrl: 'pagamento.html'
})
export class PagamentoPage {

  @ViewChild(Content) content: Content;
  @ViewChild('card') card;
  @ViewChild('month') month;
  @ViewChild('year') year;
  @ViewChild('ccv') ccv;

  private form: FormGroup;
  private formc: FormGroup;
  private submitAttempt: boolean = false;
  public show_brand: boolean = false;
  public brand_card: string;
  private cc: number = 0;
  private currShipping: number = 0.00;
  private currTotal: number = 0.00;
  private currLogistic: number = 0;
  private currCouponID: number = 0;
  private currCoupon: number = 0.00;
  private realTotal: number = 0.00;
  public wallet_fund: number = 0.00;
  public msg_wallet_fund: string = "O saldo de sua Carteira Virtual é insuficiente para pagar este pedido. Utilize outra forma de pagamento!";
  public btn_wallet_fund: boolean = true;

  constructor(public navCtrl: NavController,  
              private globals: GlobalVars,             
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              private pagseg: PaymentProvider, 
              private storage: Storage,
              private cartProvider: CartProvider,
              private toastCtrl: ToastController,
              public navParams: NavParams, 
              public utils: UtilsProvider,
              private formBuilder: FormBuilder, 
              public platform: Platform) 
    {
    
    this.form = this.formBuilder.group({
      nameCard: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      numCard: ['', Validators.compose([Validators.required])],
      mesValidadeCard: ['', Validators.compose([Validators.required])],
      anoValidadeCard: ['', Validators.compose([Validators.required])],
      codSegCard: ['', Validators.compose([Validators.maxLength(3), Validators.minLength(3), Validators.pattern('[0-9_]*'), Validators.required])],
      parcelas: ['', Validators.compose([Validators.required])],
      cpf: ['', Validators.compose([this.checkCPF("cpf")])],
    });

    this.formc = this.formBuilder.group({
      mycoupon: ''
    });

    this.globals.isHomePage = false;
    this.globals.showFooterOnSomePages = false;
 
    this.pagseg.iniciar(this.globals.pagseg_email, this.globals.pagseg_token);
  }

  focus(currField, field){
    var lenCurrField = this.form.controls[currField].value;
    if (currField == "numCard" && lenCurrField.length == 19){
      this.month.setFocus();
    }
    if (currField == "mesValidadeCard" && lenCurrField.length == 2){
      this.year.setFocus();
    }
    if (currField == "anoValidadeCard" && lenCurrField.length == 4){
      this.ccv.setFocus();
    }
  }
   
  changePayment(val){
    if (val == 'CREDIT_CARD'){
      this.cc = 1;
      this.scrollTo('cre');
    }else if (val == 'boleto'){
      this.cc = 2;
      this.scrollTo('bol');
    }else if (val == 'wallet'){
      this.cc = 3;
      this.scrollTo('wal');
      this.getWalletFund();
    }
    this.pagseg.dados.type_payment = val;
  }

  scrollTo(elementId: string) {
    setTimeout( () => {
      let y = document.getElementById(elementId).offsetTop;
      this.content.scrollTo(0, y + 470);
    }, 500);
  }

  addFund(){
    this.navCtrl.push(SeligapayPage, {});
  }

  getWalletFund(){
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Atualizando Saldo...'});
    loading.present();
    this.cartProvider.fund().subscribe((res) => {     
      this.wallet_fund = res.fund;
      if (this.wallet_fund >= this.realTotal)
      {
        this.msg_wallet_fund = "Você pode pagar este pedido utilizando o Se Liga Pay!<br><br>Sua Carteira Virtual para realizar compras no Se Liga Aí de forma prática, rápida e segura!<br><br>O processamento é instantâneo.<br><br>Para maiores informações sobre o uso da carteira, taxas e disponibilidade, consulte nossos <a target='_blank' href='https://em2d.com.br/privacy-policy.html'>Termos de Uso</a>.";
        this.btn_wallet_fund = false;
      }else{
        this.msg_wallet_fund = "O saldo de sua Carteira Virtual do Se Liga Pay! é insuficiente para pagar este pedido. Utilize outra forma de pagamento!";
        this.btn_wallet_fund = true;
      }
      loading.dismiss();
    }, error => {
      this.wallet_fund = 0.00;
      this.msg_wallet_fund = "O saldo de sua Carteira Virtual do Se Liga pay! é insuficiente para pagar este pedido. Utilize outra forma de pagamento!";
      this.btn_wallet_fund = true;
      loading.dismiss();
    });
  }

  getCoupon(){
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Buscando cupom...'});
    loading.present();
    var mycoupon = this.formc.controls["mycoupon"].value;
    this.cartProvider.coupon({cod: mycoupon}).subscribe((res) => {  
      if (res.couid == 0)
      {
        this.formc.controls["mycoupon"].setValue("");
        this.presentToast('Cupom inválido ou inexistente.');
      }   
      this.currCouponID = res.couid;
      this.currCoupon = res.coupon; 
      this.currTotal = this.realTotal + res.coupon;
      loading.dismiss();
    }, error => {
        this.presentToast('Não foi possível obter seu cupom de desconto.');
        loading.dismiss();  
    });
  }

  ionViewWillEnter(){
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Aguarde...'});
    loading.present();
    var json_cart = [];
    var tot = 0;
    return this.storage.forEach( (value: string, key: string, iterationNumber: Number) => {
      if (value !== undefined){
        var only_cart_keys = key.split("-", 1);
        if (only_cart_keys[0] === "c" || only_cart_keys[0] === "o"){
          var json = JSON.parse(JSON.stringify(value));
          tot += (json.qtd * json.vlunit);
          json_cart.push(json);
        }
      }
    })
    .then(() => {
      this.cartProvider.shipping({item: json_cart}).subscribe((res) => {
        this.currShipping = res.shipping;
        this.currTotal = tot + res.shipping;
        this.realTotal = this.currTotal;
        this.currLogistic = res.logid;
        loading.dismiss();
      }, error => {
          //this.ionViewWillEnter();
          loading.dismiss();  
      });
      return Promise.resolve(json_cart);
    })
    .catch((error) => {
      //this.ionViewWillEnter();
      return Promise.reject(error);
    });
  }

  basicAlert(options: {title, message, buttons}) {
    let alert = this.alertCtrl.create(options);
    alert.present();
  }

  isRequired(){
    if (this.cc != 1){
      this.form.controls["nameCard"].setValue("NONAME");
      this.form.controls["numCard"].setValue("0000000000000000");
      this.form.controls["cpf"].setValue("00000000191");
      this.form.controls["mesValidadeCard"].setValue("00");
      this.form.controls["anoValidadeCard"].setValue("0000");
      this.form.controls["codSegCard"].setValue("000");
      this.form.controls["parcelas"].setValue("0");
    }
  }

  checkCPF(field_name): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      var cpfcnpj = control.root.value[field_name];
      var input = control.value;
      cpfcnpj = input;
      if (cpfcnpj != undefined){
        cpfcnpj = cpfcnpj.replace(/[^\d]+/g,'');
        if (cpfcnpj.length == 11){
          if(this.utils.validCPF(cpfcnpj)){
            return null;
          }else{
            return {'checkCPF' : false};
          }
        }
        if (cpfcnpj.length != 11){
            return {'checkCPF' : false};
        }
      }else{
        return null;
      }
    }
  }

  detectCardType(cur_val) 
  {
    var cur_val = this.form.controls["numCard"].value;
    var jcb_regex = new RegExp('^(?:2131|1800|35)[0-9]{0,}$'); //2131, 1800, 35 (3528-3589)
    var amex_regex = new RegExp('^3[47][0-9]{0,}$'); //34, 37
    var diners_regex = new RegExp('^3(?:0[0-59]{1}|[689])[0-9]{0,}$'); //300-305, 309, 36, 38-39
    var visa_regex = new RegExp('^4[0-9]{0,}$'); //4
    var mastercard_regex = new RegExp('^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[01]|2720)[0-9]{0,}$'); //2221-2720, 51-55
    var maestro_regex = new RegExp('^(5[06789]|6)[0-9]{0,}$'); //always growing in the range: 60-69, started with / not something else, but starting 5 must be encoded as mastercard anyway
    var discover_regex = new RegExp('^(6011|65|64[4-9]|62212[6-9]|6221[3-9]|622[2-8]|6229[01]|62292[0-5])[0-9]{0,}$');
    var sel_brand = "unknown";

    cur_val = cur_val.replace(/\D/g, '');
    
    if (cur_val.match(jcb_regex)) {
      sel_brand = "jcb";
    } else if (cur_val.match(amex_regex)) {
      sel_brand = "amex";
    } else if (cur_val.match(diners_regex)) {
      sel_brand = "diners-club";
    } else if (cur_val.match(visa_regex)) {
      sel_brand = "visa";
    } else if (cur_val.match(mastercard_regex)) {
      sel_brand = "mastercard";
    } else if (cur_val.match(discover_regex)) {
      sel_brand = "discover";
    } else if (cur_val.match(maestro_regex)) {
      if (cur_val[0] == '5') { 
        sel_brand = "mastercard";
      } else {
        sel_brand = "maestro"; 
      }
    }

    if (sel_brand != "unknown"){
      this.show_brand = true;
    }else{
      this.show_brand = false;
    }
  
    this.brand_card = "assets/icon/cards/" + sel_brand + ".png";
  }

  checkPayment(){
    this.isRequired();
    this.submitAttempt = true;
    if(this.form.valid){
      this.pagseg.dados.nome = this.form.controls["nameCard"].value;
      var card = this.form.controls["numCard"].value;
      var cpf = this.form.controls["cpf"].value;
      this.pagseg.dados.numCard = card.replace(/\./g,'');
      this.pagseg.dados.cpf = cpf.replace(/[^\d]+/g,'');
      this.pagseg.dados.mesValidadeCard = this.form.controls["mesValidadeCard"].value;
      this.pagseg.dados.anoValidadeCard = this.form.controls["anoValidadeCard"].value;
      this.pagseg.dados.codSegCard = this.form.controls["codSegCard"].value;
      var installment = this.form.controls["parcelas"].value;
      var arrInstallment = installment.split('|');
      this.pagseg.dados.numparcela = arrInstallment[0];
      this.pagseg.dados.valparcela = arrInstallment[1];
      this.pagseg.dados.ord_value = this.realTotal + this.currCoupon;
      this.pagseg.dados.shipping = this.currShipping;
      this.pagseg.dados.couid = this.currCouponID;
      this.pagseg.dados.coupon = this.currCoupon;
      this.pagseg.dados.logid = this.currLogistic;
      this.pagseg.pagar('saveorder');
    }
  }

  /**
   * Method to show Toast messages 
   * on bottom of device
   * @param msgToast 
   */
  presentToast(msgToast) 
  {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 4500,
      position: 'bottom'
    });
    toast.present();
  }
}
