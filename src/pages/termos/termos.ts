import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TermsProvider } from '../../providers/terms';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { GlobalVars } from '../../providers/globalVars';

@Component({
  selector: 'page-termos',
  templateUrl: 'termos.html'
})
export class TermosPage {

   content: String;
   updated_at: String;

  constructor(
    private loadingCtrl: LoadingController,
    private globals: GlobalVars,
    private termsProvider: TermsProvider, 
    public navCtrl: NavController) {

      this.globals.isHomePage = false;
      this.globals.showFooterOnSomePages = false;
    }

  ionViewWillEnter() {
     let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Buscando termos...'});
    loading.present();
    this.termsProvider.terms()
      .subscribe(data => {
        this.content = data.content;
        this.updated_at = "<small>Atualizado em " + data.updated_at + "</small>";
        loading.dismiss();
      },
      error => { 
        loading.dismiss();
        this.ionViewWillEnter();
      });
  }

}
