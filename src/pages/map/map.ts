import { Component, NgZone } from '@angular/core';
import { NavController, AlertController, NavParams, ToastController } from 'ionic-angular';
//import { IonicPage } from 'ionic-angular/navigation/ionic-page';
import { Geolocation } from '@ionic-native/geolocation';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { GeolocationsProvider } from '../../providers/geolocations';
import { LaunchNavigatorOptions, LaunchNavigator } from '@ionic-native/launch-navigator';
import { GlobalVars } from '../../providers/globalVars';

declare var google;

//@IonicPage()

@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPage {

  map: any;
  infoWindows: any;
  directionsService = new google.maps.DirectionsService();
  directionsDisplay = new google.maps.DirectionsRenderer();
  startPosition: any;
  originPosition: string;
  destinationPosition: string;
  coodinate;
  cLat: any = null;
  cLng: any = null;
  cName: any = null;
  cAdd: any = null;
  cOffe: any = null;
  cIcon: any = null;

  constructor(private toastCtrl: ToastController, 
              private geolocation: Geolocation,
              public navParams: NavParams,
              private globals: GlobalVars,
              private loadingCtrl: LoadingController, 
              public zone: NgZone, 
              private launchNavigator: LaunchNavigator,
              private geolocationsProvider: GeolocationsProvider,
              public navCtrl: NavController) {

                this.globals.isHomePage = false;
                this.globals.showFooterOnSomePages = true;

                this.cLat = this.navParams.get('cLat');
                this.cLng = this.navParams.get('cLng');
                this.cName = this.navParams.get('cName'); 
                this.cAdd = this.navParams.get('cAdd');
                this.cOffe = this.navParams.get('cOffe');
                this.cIcon = this.navParams.get('cIcon');

                (window as any).angularComponent = { goToStore: this.goToStore, zone: zone };
                this.infoWindows = [];
                this.coodinate = {
                  latitude: '', 
                  longitude: ''
                }
  }

  ionViewWillEnter() {
    this.initializeMap()
  }

  goToStore = (address) => {
    this.zone.run(() => {
      let options: LaunchNavigatorOptions = {
        appSelection: {
          dialogHeaderText: "Navegar até o local usando...",
          cancelButtonText: "Cancelar",
          rememberChoice: {
            enabled: false,
            prompt: {
              headerText: "Navegar até o local",
              bodyText: "Utilizar este App na próxima vez?",
              yesButtonText: "SIM",
              noButtonText: "NÃO"
            }
          }
      }};
      this.launchNavigator.navigate(address, options)
        .then(
          success => console.log('Launched navigator'),
          error => console.log('Error launching navigator', error)
        );
    });
  }

  initializeMap() {

   let loading = this.loadingCtrl.create({
    spinner: 'crescent',
    content: 'Carregando mapa...'
  });
  loading.present();

  const dayMap = [
    {
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#ebe3cd"
        }
      ]
    },
    {
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#523735"
        }
      ]
    },
    {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#f5f1e6"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#c9b2a6"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#dcd2be"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#ae9e90"
        }
      ]
    },
    {
      "featureType": "landscape.natural",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [
        {
          /*"color": "#dfd2ae"*/
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
        /*  "color": "#93817c" */
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry.fill",
      "stylers": [
        {
        /*  "color": "#a5b076" */
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          /* "color": "#447530" */
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#f5f1e6"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#fdfcf8"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#f8c967"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#e9bc62"
        }
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#e98d58"
        }
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#db8555"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#806b63"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#8f7d77"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#ebe3cd"
        }
      ]
    },
    {
      "featureType": "transit.station",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#dfd2ae"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#b9d3c2"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#92998d"
        }
      ]
    }
  ];

  const nightMap = [
    {
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#242f3e"
        }
      ]
    },
    {
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#746855"
        }
      ]
    },
    {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#242f3e"
        }
      ]
    },
    {
      "featureType": "administrative.locality",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#d59563"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          /* "color": "#d59563" */
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [
        {
          /* "color": "#263c3f" */
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          /* "color": "#6b9a76" */
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#38414e"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#212a37"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#9ca5b3"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#746855"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#1f2835"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#f3d19c"
        }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#2f3948"
        }
      ]
    },
    {
      "featureType": "transit.station",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#d59563"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#17263c"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#515c6d"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#17263c"
        }
      ]
    }
  ];

  var timeNow = new Date().getTime();
  var twoHoursIntoFuture = new Date(timeNow + (60 * 60));
  var currentHour = (twoHoursIntoFuture.getHours());

      if (this.cLat != null && this.cLng != null){

              this.startPosition = new google.maps.LatLng(parseFloat(this.cLat), parseFloat(this.cLng));
      
              const mapOptions = {
                zoom: 18,
                center: this.startPosition,
                disableDefaultUI: true,
                styles: (currentHour > 5 && currentHour < 19 ? dayMap : nightMap)
              }
          
              this.map = new google.maps.Map(document.getElementById('map'), mapOptions);
              this.directionsDisplay.setMap(this.map);
          
              var period = (currentHour > 5 && currentHour < 19 ? 'day' : 'night');
              var data = [{
                latitude: this.cLat,
                longitude: this.cLng,
                branchname: this.cName,
                address: this.cAdd,
                offers: this.cOffe,
                icon: this.cIcon
              }];
              this.addMarkersToMap(data, period);
              loading.dismiss();

      }else{

        var newlat = localStorage.getItem('newlat');
        var newlng = localStorage.getItem('newlng');
      
            if (newlat != "0" && newlng != "0" && newlat != null && newlng != null){
      
              this.coodinate.latitude = parseFloat(newlat);
              this.coodinate.longitude = parseFloat(newlng);
              this.startPosition = new google.maps.LatLng(parseFloat(newlat), parseFloat(newlng));
             
      
              const mapOptions = {
                zoom: 13,
                center: this.startPosition,
                disableDefaultUI: true,
                styles: (currentHour > 5 && currentHour < 19 ? dayMap : nightMap)
              }
          
              this.map = new google.maps.Map(document.getElementById('map'), mapOptions);
              this.directionsDisplay.setMap(this.map);
          
              /* const marker = new google.maps.Marker({
                position: this.startPosition,
                map: this.map,
              }); */
              loading.dismiss();
              this.getMarkers(currentHour > 5 && currentHour < 19 ? 'day' : 'night');
      
      
            }else{
      
              this.geolocation.getCurrentPosition() 
                .then((resp) => {
                  this.coodinate.latitude = resp.coords.latitude;
                  this.coodinate.longitude = resp.coords.longitude;
                  this.startPosition = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
                 
      
                  const mapOptions = {
                    zoom: 13,
                    center: this.startPosition,
                    disableDefaultUI: true,
                    styles: (currentHour > 5 && currentHour < 19 ? dayMap : nightMap)
                  }
              
                  this.map = new google.maps.Map(document.getElementById('map'), mapOptions);
                  this.directionsDisplay.setMap(this.map);
              
                  /* const marker = new google.maps.Marker({
                    position: this.startPosition,
                    map: this.map,
                  }); */
                  loading.dismiss();
                  this.getMarkers(currentHour > 5 && currentHour < 19 ? 'day' : 'night');
                }).catch((error) => {

                  this.presentToast('Erro ao recuperar sua posição' + error);
                  
                }); 
      
            }

      }

 
     
  }

  addInfoWindowToMarker(marker) {
    var infoWindowContent = '<p><strong>' + marker.title + '</strong><br>' + marker.offers + '</p>' +
                            '<p>' + marker.address + '</p>' + 
                            '<p onclick="window.angularComponent.goToStore(\'' + marker.address + '\')" style="font-size: 4.5vw; border-radius: 5px; background-color:#9c2020; text-align: center; line-height: 35px; max-width: 100%; vertical-align: middle; color: white; font-weight:bold;">IR ATÉ LÁ</p>';

    var infoWindow = new google.maps.InfoWindow({
      content: infoWindowContent
    });
    marker.addListener('click', () => {
      this.closeAllInfoWindows();
      infoWindow.open(this.map, marker);
    });
    this.infoWindows.push(infoWindow);
  }

  closeAllInfoWindows() {
    for(let window of this.infoWindows) {
      window.close();
    }
  }

  getMarkers(period) {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Por favor, aguarde...'});
    loading.present();
    this.geolocationsProvider.geolocations(this.coodinate)
      .subscribe(data => {
        this.addMarkersToMap(data, period);
        loading.dismiss();
      },
      error => { 
        this.presentToast("Não foi possível obter os locais no mapa.");
        loading.dismiss();
        
      });
  }

  addMarkersToMap(markers, period) {
    
    if (markers !== null){
      for(let marker of markers) {
        var position = new google.maps.LatLng(marker.latitude, marker.longitude);
        var markersOnMap = new google.maps.Marker({
          position: position,
          title: marker.branchname,
          address: marker.address,
          offers: marker.offers,
          icon: 'https://em2d.com.br/seligaai/icons/' + period + '/' + marker.icon + '.png'}); //reduzir tamanho icone
          markersOnMap.setMap(this.map);
        this.addInfoWindowToMarker(markersOnMap);
      }
    }  
  }

  /**
   * Method to show Toast messages 
   * on bottom of device
   * @param msgToast 
   */
  presentToast(msgToast) 
  {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 4500,
      position: 'bottom'
    });
    toast.present();
  }

}
