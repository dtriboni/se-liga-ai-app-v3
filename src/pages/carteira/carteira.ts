import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { CartProvider } from '../../providers/cart';
import { GlobalVars } from '../../providers/globalVars';
import { SeligapayPage } from '../pagamento/seligapay';
import { ContratoPage } from './contrato';

@Component({
  selector: 'page-carteira',
  templateUrl: 'carteira.html'
})
export class CarteiraPage {

  wallet = [];
  fund: number = 0;
  show_hide: string = "eye-off";
  view_fund: boolean = false;
  first_usage_wallet: any;

  constructor(private toastCtrl: ToastController, 
    private loadingCtrl: LoadingController, 
    private cartProvider: CartProvider,
    public modalCtrl: ModalController,
    private globals: GlobalVars,
    public navCtrl: NavController) {

      this.view_fund = false;
      this.globals.isHomePage = false;
      this.globals.showFooterOnSomePages = false;
      this.first_usage_wallet = localStorage.getItem('first_usage_wallet');
    }


   showHideFund(){
    if (this.view_fund == false)
        this.view_fund = true;
    else if (this.view_fund == true)
        this.view_fund = false;
  }

  ionViewWillEnter() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Aguarde...'});
    loading.present();
    this.cartProvider.extract().subscribe(data => {
        
        this.wallet = data;
        this.fund = data[0].fund;
        loading.dismiss();

    },error => { 
       
        loading.dismiss();
        this.fund = 0;
        
      });
  }

  addFund(){
    if (this.first_usage_wallet == "signed_ok")
    {
      this.navCtrl.push(SeligapayPage, {});
      
    }else{
      let profileModal = this.modalCtrl.create(ContratoPage, {});
      profileModal.onDidDismiss(data => {
        if (data.first_usage_wallet == "signed_ok")
        {
          localStorage.setItem('first_usage_wallet', 'signed_ok');
          this.first_usage_wallet = data.first_usage_wallet;
          this.navCtrl.push(SeligapayPage, {});
        }
      });
      profileModal.present();
    }
  }  

}
