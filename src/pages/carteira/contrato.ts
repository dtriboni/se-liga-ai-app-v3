import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { GlobalVars } from '../../providers/globalVars';
import { TermsProvider } from '../../providers/terms';

@Component({
  selector: 'page-contrato',
  templateUrl: 'contrato.html',
})
export class ContratoPage {

    content: any;
    updated_at: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private loadingCtrl: LoadingController,
    private termsProvider: TermsProvider,
    private globals: GlobalVars
    
  ) {
    this.globals.isHomePage = false;
    this.globals.showFooterOnSomePages = true;

    let loading = this.loadingCtrl.create({
        spinner: 'crescent',
        content: 'Aguarde...'});
    loading.present();
    this.termsProvider.terms()
    .subscribe(data => {
        this.content = data.slp_content;
        this.updated_at = "<small>Atualizado em " + data.updated_at + "</small>";
        loading.dismiss();
    },
    error => { 
        loading.dismiss();
        
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  btOk(){
    let data = { 'first_usage_wallet': 'signed_ok' };
    this.viewCtrl.dismiss(data);
  }

}
