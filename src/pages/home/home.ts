import { Component, NgZone } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { NavController, LoadingController, NavParams, Platform, Refresher, ToastController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth';
import { Facebook } from '@ionic-native/facebook';
import { TermosPage } from '../termos/termos';
import { MapPage } from '../map/map';
import { AlertasPage } from '../alertas/alertas';
import { LojaPage } from '../lojas/loja';
import { StoresProvider } from '../../providers/stores';
import { GlobalVars } from '../../providers/globalVars';
import { IndicalojaPage } from '../indicacoes/loja';
import { Geolocation } from '@ionic-native/geolocation';
import { SocialSharing } from '@ionic-native/social-sharing';
import { DivulgaPage } from '../divulga/divulga';
import { AlteracaoPage } from '../cadastro/alteracao';
import { CategoriesPage } from '../categorias/categorias';
import { DestaquesPage } from '../destaques/destaques';
import { SorteiosPage } from '../sorteios/sorteios';
import { FavoritosPage } from '../favoritos/favoritos';
import { Observable, Subscription } from 'rxjs';
import { CacheService } from '../../app/cache.service';
import { Cache } from '../../app/cache';
import { LoginPage } from '../login/login';
import { Clipboard } from '@ionic-native/clipboard';
import { LojasPage } from '../lojas/lojas';
import { PedidosPage } from '../pedidos/pedidos';
import { CartProvider } from '../../providers/cart';
import { VerprodutoPage } from '../verproduto/verproduto';
import { FormControl } from '@angular/forms';
import { SearchProvider } from '../../providers/search';
import { PaymentProvider } from '../../providers/payment';

declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  animations: [
    trigger('visibilityChanged', [
      state('shown', style({ opacity: 1 })),
      state('hidden', style({ opacity: 0 })),
      transition('* => *', animate('500ms'))
    ])
  ]
})
export class HomePage {

  isShown = true;
  imgLoading = "assets/imgs/picture.png";
  visibility: string = 'hidden';
  logged: string;
  usr_name: string;
  usr_email: string;
  usr_signup: string;
  usr_picture: string;
  usr_newaddress: string; 
  searchTerm: string = '';
  searchControl: FormControl;
  items = [];
  stores = [];
  sellers = [];
  coupons = [];
  banners = [];
  catID: any;
  catName: string;
  startPosition: any;

  searching: any = false;
  noitem:boolean = true;
  noitemstore:boolean = false;
  noitemseller:boolean = false;
  noitemcoupon: boolean = false;
  noitembanner: boolean = false;
  showmoreseller: boolean = false;
  loadingstore: boolean = true;
  loadingseller: boolean = true;
  loadingcoupon: boolean = true;
  loadingbanner: boolean = true;

  coordinate;
  data: any;
  errorMessage: string;
  page = 1;
  perPage = 0;
  totalData = 0;
  totalPage = 0;

  autocompleteItems;
  autocomplete;
  showSearchLocation: boolean = false;
  showitem: boolean = false;
  service = new google.maps.places.AutocompleteService();
  geocoder = new google.maps.Geocoder();

  showFind: boolean = false;
  newped: boolean = false;
  countFars: number = 0;
  faraway: boolean = false;
  loaded: boolean = false;
  t_Out;

  //public data$: Observable<HomePage>;
  //private cache: Cache<StoresProvider>;
  //private cacheSubscription: Subscription;

  constructor(public navParams: NavParams,
              private authLogout: AuthProvider, 
              public cacheService: CacheService,
              private clipboard: Clipboard,
              private loadingCtrl: LoadingController, 
              private toastCtrl: ToastController,
              private globals: GlobalVars,
              public geolocation: Geolocation,
              private zone: NgZone, 
              private storesProvider: StoresProvider,
              private cartProvider: CartProvider,
              private searchProvider: SearchProvider,
              private paymentProvider: PaymentProvider,
              private socialSharing: SocialSharing,
              private facebook: Facebook,
              private platform: Platform,
              public navCtrl: NavController) {
     
      this.globals.isHomePage = true;
      this.globals.showFooterOnSomePages = true;
      this.usr_name = localStorage.getItem('usr-name');
      this.usr_email = localStorage.getItem('usr-email');
      this.usr_signup = localStorage.getItem('logged');
      this.usr_picture = localStorage.getItem('usr-picture');
      this.catID = this.navParams.get('catID');
      this.catName = this.navParams.get('catName');
      this.newped = this.navParams.get('newped');


      this.coordinate = {
        latitude: '', 
        longitude: ''
      }

      this.autocompleteItems = [];
      this.autocomplete = {
        query: ''
      };

      this.searchControl = new FormControl();

      if (this.usr_signup == 'email'|| this.usr_signup == 'facebook')
      {
        this.paymentProvider.statusPayment(this.globals.pagseg_email, this.globals.pagseg_token).subscribe(data => {
        }, error => {
        }) ;
      }
  }

  copyText(txt, store){
    //$ ionic cordova plugin add cordova-clipboard
    //$ npm install --save @ionic-native/clipboard@3
    this.clipboard.copy(txt);
    var local = (store == "0" ? "Utilize-o em sua compra no Se Liga Aí." : "Utilize-o em sua compra na loja no Se Liga Aí.");
    this.presentToast("Cupom " + txt + " copiado! " + local);
  }

  pasteText(){
    this.clipboard.paste().then(
      (resolve: string) => {
         alert(resolve);
       },
       (reject: string) => {
         alert('Error: ' + reject);
       }
     );
  }

  checkFocus(){
    this.isShown = false;
  }

  checkBlur() {
    this.isShown = false;
  }

  registerPage() {
    this.navCtrl.push(AlteracaoPage, {});
  }

  showAllSellers(){
    this.navCtrl.push(LojasPage, {isSellers: true, catName: "Todos os Vendedores"});
  }

  categoriesPage() {
    this.navCtrl.push(CategoriesPage, {});
  }

  alertasPage() {
    this.navCtrl.push(AlertasPage, {});
  }

  highlightsPage(){
    this.navCtrl.push(DestaquesPage, {});
  }

  favouritesPage(){
    this.navCtrl.push(FavoritosPage, {});
  }

  indicaPage() {
    this.navCtrl.push(IndicalojaPage, {});
  }

  cependPage() {
    this.showFind = false;
    if (this.showSearchLocation == false)
      this.showSearchLocation = true;
    else if (this.showSearchLocation == true)
      this.showSearchLocation = false;  
  }

  ordersPage(){
    this.navCtrl.push(PedidosPage, {});
  }

  searchPage() {
    this.showSearchLocation = false;  
    if (this.showFind == false)
      this.showFind = true;
    else if (this.showFind == true)
      this.showFind = false;
  }

  lotteryPage(){
    this.navCtrl.push(SorteiosPage, {});
  }

  signPage(){
    this.navCtrl.push(LoginPage, {});
  }

  termosPage() {
    this.navCtrl.push(TermosPage, {});
  }

  mapsPage() {
    this.navCtrl.push(MapPage, {});
  }

  divulgaPage(){
    this.navCtrl.push(DivulgaPage, {});
  }

  getProductDetails(id, store, starts){
    if (starts > 0){
      this.presentToast("Este desconto ainda não está disponível! Aguarde a sua data de início.");
    }else{
      this.navCtrl.push(VerprodutoPage, {ofeID: id, strName: store});
    }
  }

  getStoreDetails(id, store){
    this.navCtrl.push(LojaPage, {strID: id, strName: store});
  }

  shareFriend(){
    this.socialSharing.shareViaWhatsApp('Conheça o Se Liga Aí! Melhor App de ofertas e promoções do Brasil.', null, 'https://em2d.com.br/#seligaai')
  }

  rateApp()
  {
    if (this.platform.is('android')) {
      location.href="https://play.google.com/store/apps/details?id=br.app.seligaai";
    }else if (this.platform.is('ios')) {
      location.href="https://itunes.apple.com/br/app/seligaai/id911795091";   
    }
  }

  exitMyApp()
  {
    let loading = this.loadingCtrl.create({
        spinner: 'crescent',
        content: 'Saindo...'
    });
    loading.present();
    
    this.authLogout.logout().subscribe(data => {

      var val = localStorage.getItem('logged');
      if (val === 'facebook')
      {
        this.facebook.logout().then( res => {
          localStorage.setItem('logged', 'false');
          localStorage.clear();
          this.platform.exitApp();
        }).catch(e => {
          localStorage.setItem('logged', 'false');
          localStorage.clear();
          this.platform.exitApp();
        }); 

      }else{

        localStorage.setItem('logged', 'false');
        localStorage.clear();
        this.platform.exitApp();
      }
      loading.dismiss();

    }, error => {

      localStorage.setItem('logged', 'false');
      localStorage.clear();
      this.platform.exitApp();
      loading.dismiss();
      
    });
  }

  doRefresh(refresher) 
  {
    this.ionViewWillEnter(false);
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

 /*  doRefresh(refresher: Refresher): void {
    // Check if the cache has registered.
    if (this.cache) {
      this.cache.refresh().subscribe(() => {
        // Refresh completed, complete the refresher animation.
        refresher.complete();
      }, (err) => {
        // Something went wrong! 
        // Log the error and cancel refresher animation.
        console.error('Refresh failed!', err);
        refresher.cancel();
      });
    } else {
      // Cache is not registered yet, so cancel refresher animation.
      refresher.cancel();
    }
  } */

  doInfinite(infiniteScroll) {
    this.page = this.page+1;
    setTimeout(() => {
       this.storesProvider.stores(this.coordinate, this.page).subscribe(
           res => {
             this.data = res;
             this.perPage = this.data.per_page;
             this.totalData = this.data.total;
             this.totalPage = this.data.total_pages;
             for(let i=0; i<this.data.stores.length; i++) {
               this.stores.push(this.data.stores[i]);
             }
           },
           error =>  this.errorMessage = <any>error);
      infiniteScroll.complete();
    }, 1000);
  }


  ionViewWillEnter(load: boolean = true) 
  {
    if (this.t_Out != null && this.t_Out != undefined)
      clearTimeout(this.t_Out);
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Aguarde...'});
      if (load)   
        loading.present();
    this.page = 1;
    this.faraway = true;
    this.loaded = false;
    this.countFars = 0;
    this.totalPage = 0;
    this.showitem = false;
    this.showSearchLocation = false;
    this.usr_newaddress = localStorage.getItem('newadd');
    var newlat = localStorage.getItem('newlat');
    var newlng = localStorage.getItem('newlng');

    this.globals.showFooterOnSomePages = true;

    if (newlat != "0" && newlng != "0" && newlat != null && newlng != null)
    {
        this.globals.latitude = parseFloat(newlat);
        this.globals.longitude = parseFloat(newlng);
        this.coordinate.latitude = newlat;
        this.coordinate.longitude = newlng;
        
        this.storesProvider.stores(this.coordinate, this.page).subscribe(async data => 
        {
            this.countFars = 0;
            this.stores = data.stores;
            this.data = data;
            this.noitemstore = false;
            this.loadingstore = false;
            this.perPage = this.data.per_page;
            this.totalData = this.data.total;
            this.totalPage = this.data.total_pages;
            if (this.totalData == 0){
              this.faraway = true;
             /*  this.t_Out = setTimeout(() => {
                this.cependPage();
              }, 25000); */
              loading.dismiss();
            }else{
              await this.callSellers(this.coordinate);
              await this.callCoupons();
              await this.callBanners();
              this.faraway = false;
              loading.dismiss();
            }
            this.loaded = true;
            
        },error => {

            this.countFars = 1;
            this.loadingstore = false;
            this.noitemstore = true;
            this.faraway = true;
            /* this.t_Out = setTimeout(() => {
              this.cependPage();
            }, 25000); */
            loading.dismiss();
            this.loaded = true;
        }); 
 
        /* let storesListObservable: Observable<StoresProvider> = this.storesProvider.stores(this.coordinate, this.page);
        this.cacheService
          .register('home', storesListObservable)
          .mergeMap((cache: Cache<StoresProvider>) => cache.get())
          .subscribe((stores) => {
            this.data = stores;
            this.perPage = this.data.per_page;
            this.totalData = this.data.total;
            this.totalPage = this.data.total_pages;
            for(let i=0; i<this.data.stores.length; i++) {
              this.stores.push(this.data.stores[i]);
            }
        }); */

    }else{

      const watch = this.geolocation.watchPosition().subscribe(position => 
      {
        if(position.coords != null)
        {
          this.globals.latitude = position.coords.latitude;
          this.globals.longitude = position.coords.longitude;
          this.coordinate.latitude = position.coords.latitude;
          this.coordinate.longitude = position.coords.longitude;
          this.storesProvider.stores(this.coordinate, this.page).subscribe(async data => 
          {
              this.countFars = 0;
              this.stores = data.stores;
              this.data = data;
              this.noitemstore = false;
              this.loadingstore = false;
              this.perPage = this.data.per_page;
              this.totalData = this.data.total;
              this.totalPage = this.data.total_pages;
              if (this.totalData == 0){
                
                this.faraway = true;
                /* this.t_Out = setTimeout(() => {
                  this.cependPage();
                }, 25000); */
                loading.dismiss();

              }else{
                await this.callSellers(this.coordinate);
                await this.callCoupons();
                await this.callBanners();
                this.faraway = false;
                loading.dismiss();
              }
              watch.unsubscribe();
              this.loaded = true;
          
          }, error => {

              watch.unsubscribe();
              this.countFars = 1;
              this.noitemstore = true;
              this.loadingstore = false;
              loading.dismiss();
              this.faraway = true;
              /* this.t_Out = setTimeout(() => {
                this.cependPage();
              }, 25000); */
              this.loaded = true;

          }); 

            

             
          
            /* let storesListObservable: Observable<StoresProvider> = this.storesProvider.stores(this.coordinate, this.page);

            this.cacheSubscription = this.cacheService
              .register('homess', storesListObservable)
              .mergeMap((cache: Cache<StoresProvider>) => {
                this.cache = cache;
                this.cache.refresh().subscribe();
                return this.cache.get$;
              })
              .subscribe((stores) => {
                watch.unsubscribe();
                this.data = stores;
                this.perPage = this.data.per_page;
                this.totalData = this.data.total;
                this.totalPage = this.data.total_pages;
                for(let i=0; i<this.data.stores.length; i++) {
                  this.stores.push(this.data.stores[i]);
                }

                [].forEach.call(document.querySelectorAll('img[data-src].image-container'), function(img) {
                  img.setAttribute('src', img.getAttribute('data-src'));
                  img.onload = function() {
                    img.removeAttribute('data-src');
                  };
                });

            }, error => {
              //show view error
              watch.unsubscribe();
              this.noitem = true;
            });  */
        }else{
          loading.dismiss();
          this.ionViewWillEnter();
        }
        
      }); 

    }
  }


setFilteredItems() {
  var val = this.searchTerm;
  if (val && val.trim() != '' && val.length > 2 && val != null) {
    this.searching = true;
    this.searchProvider.search(val) //preferencias de pesquisa e limit
    .subscribe(res => {
        this.searching = false;
        this.items = res;
        if (res == null)
          this.noitem = true;
        else
          this.noitem = false;
    }, error => {
        this.searching = false;
        this.noitem = true;
        this.presentToast("Não foi possível processar sua busca. Nenhum ítem encontrado!");
    });
  }else{
    this.items = null;
    this.noitem = true;
    this.searching = false;
  }
}

  callSellers(coords){
    this.storesProvider.sellers(coords, -1).subscribe(data => 
    {
        this.sellers = data.sellers;
        this.noitemseller = false;
        this.showmoreseller = true;
        this.loadingseller = false;  
    },error => {
        this.noitemseller = true;
        this.showmoreseller = false;
        this.loadingseller = false;
    });
  }

  callCoupons(){
    this.cartProvider.coupons(0).subscribe(data => 
    {
      this.coupons = data.coupons;
      this.noitemcoupon = false;
      this.loadingcoupon = false;                  
    },error => {
      this.countFars += 1;    
      this.noitemcoupon = true;
      this.loadingcoupon = false;    
    }); 
  }

  callBanners(){
    this.storesProvider.banners().subscribe(data => 
    {
      this.banners = data;
      this.noitembanner = false;
      this.loadingbanner = false;                  
    },error => { 
      this.noitembanner = true;
      this.loadingbanner = false;    
    }); 
  }

  chooseItem(item: any) {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Aguarde...'});
    loading.present(); 
    this.geocodeAddress(item);
    let TIME_IN_MS = 3000;
      setTimeout( () => {
        loading.dismiss();
        this.autocomplete.query = '';
        this.autocompleteItems = [];
        this.ionViewWillEnter();
    }, TIME_IN_MS);
  }

  closeLocation(){
    this.showitem = false;
    this.autocomplete.query = '';
    this.autocompleteItems = [];
    this.showFind = false;
    this.showSearchLocation = false;
    this.searchTerm = null;
    this.noitem = true;
  }

  closeFind(){
    this.showitem = false;
    this.autocomplete.query = '';
    this.autocompleteItems = [];
    this.showFind = false;
    this.showSearchLocation = false;
    this.searchTerm = null;
    this.noitem = true;
  }

  clearHome(){
    this.showFind = false;
    this.showSearchLocation = false;
    this.searchTerm = null;
    this.noitem = true;
  }

  myLocation(){
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Buscando próximo à mim...'});
    loading.present(); 
    localStorage.setItem('newadd', "0");
    localStorage.setItem('newlat', "0");
    localStorage.setItem('newlng', "0");
    let TIME_IN_MS = 3000;
      setTimeout( () => {
        this.showSearchLocation = false;
        loading.dismiss();
        this.autocomplete.query = '';
        this.autocompleteItems = [];
        this.ionViewWillEnter();
    }, TIME_IN_MS);
  }
  
  updateSearch() {
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let me = this;
    me.showitem = true;
    this.service.getPlacePredictions({ input: this.autocomplete.query, componentRestrictions: {country: 'BR'} }, function (predictions, status) {
      me.autocompleteItems = []; 
      me.zone.run(function () {
        predictions.forEach(function (prediction) {
          if (prediction.terms[2] != undefined && prediction.terms[3] != undefined){
            me.autocompleteItems.push(prediction);
          }
        });
      });
    });
  }

  geocodeAddress(address) {
    this.geocoder.geocode({'address': address}, function(results, status) {
      if (status === 'OK') {
        var latlng = JSON.parse(JSON.stringify(results[0].geometry.location));
        localStorage.setItem('newadd', address);
        localStorage.setItem('newlat', latlng.lat);
        localStorage.setItem('newlng', latlng.lng);
      } else {
        alert('Erro ao buscar localização do endereço. Motivo: ' + status);
        localStorage.setItem('newadd', "0");
        localStorage.setItem('newlat', "0");
        localStorage.setItem('newlng', "0");
      }
    });
  }


  getDistance(lat2, lon2) 
  {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - this.globals.latitude);  // deg2rad below
    var dLon = this.deg2rad(lon2 - this.globals.longitude); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(this.globals.latitude)) * Math.cos(this.deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    var distance = d.toFixed(1);
    var metric = (d < 1 ? (parseFloat(distance) * 1000) + " m": distance + " km");
    if ((this.globals.latitude != 0))
      return metric.replace(".", ",");
    else
      return 'calculando...'
  }

  deg2rad(deg) 
  {
    return deg * (Math.PI/180)
  }

  /**
   * Method to show Toast messages 
   * on bottom of device
   * @param msgToast 
   */
  presentToast(msgToast) 
  {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
}