import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GlobalVars } from '../../providers/globalVars';

@Component({
  selector: 'page-divulga',
  templateUrl: 'divulga.html'
})
export class DivulgaPage {

  constructor(public navCtrl: NavController, private globals: GlobalVars) {
    this.globals.isHomePage = false;
    this.globals.showFooterOnSomePages = false;
  }

}
