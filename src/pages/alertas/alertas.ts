import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { CategoriesProvider } from '../../providers/categories';
import { GlobalVars } from '../../providers/globalVars';

@Component({
  selector: 'page-alertas',
  templateUrl: 'alertas.html'
})
export class AlertasPage {

  categories = []
  retAlerts: string = null
  parseCategories: string = null
  setCategoriesEnabled: string = null

  constructor(private toastCtrl: ToastController, 
    private loadingCtrl: LoadingController, 
    private globals: GlobalVars,
    private categoriesProvider: CategoriesProvider,
    public navCtrl: NavController) {
      this.parseCategories = localStorage.getItem('alerts');
      if (this.parseCategories != null){
        this.retAlerts = this.parseCategories;
      }
      this.globals.isHomePage = false;
  }

  ionViewDidEnter(){
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Buscando categorias...'});
    loading.present();
    this.categoriesProvider.categories()
      .subscribe(data => {
        this.categories = data;
        loading.dismiss();
      },
      error => { 
        loading.dismiss();
        this.presentToast("Não foi possível obter seus alertas.");
      });
  }

  ionViewWillEnter() {}

  updateToggleSet(whichToggle:string, ev){
    if(ev.checked === true){
      this.retAlerts += ',' + whichToggle;
    }else{
      if (this.parseCategories != null){
        var getCategories = localStorage.getItem('alerts');
        this.retAlerts = getCategories.replace(','+whichToggle, '');
      }
    }
    localStorage.removeItem('alerts');
    localStorage.setItem('alerts', this.retAlerts+'');
  }

  getToggleSet(whichToggle:string){
    var found:boolean = false;
    if (this.parseCategories != null){
      var getCategories = localStorage.getItem('alerts').split(",");
      for(var a = 0; a < getCategories.length; a++){
        if (getCategories[a] != null){
          if (getCategories[a] == whichToggle){
            found = true;
            break;
          }
        }
      }
      if (found)
        return true;
      else  
        return false;
    }else{
      return false;
    }
  }
   

  /**
   * Method to show Toast messages 
   * on bottom of device
   * @param msgToast 
   */
  presentToast(msgToast) 
  {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

}
