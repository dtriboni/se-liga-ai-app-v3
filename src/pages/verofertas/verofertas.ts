import { Component } from '@angular/core';
import { NavController, LoadingController, NavParams, ToastController } from 'ionic-angular';
import { SearchProvider } from '../../providers/search';
import { LojaPage } from '../lojas/loja';
import { GlobalVars } from '../../providers/globalVars';
import { VerprodutoPage } from '../verproduto/verproduto';

@Component({
  selector: 'page-verofertas',
  templateUrl: 'verofertas.html'
})
export class VerofertasPage {

  items = [];
  addID: any;
  strName: string;
  searchTerm: string = '';
  parseCategories: string = null

  constructor(private toastCtrl: ToastController, 
              public navCtrl: NavController, 
              public navParams: NavParams,
              public coordinates: GlobalVars,
              private loadingCtrl: LoadingController, 
              public pesquisaProvider: SearchProvider) 
  {
    this.addID = this.navParams.get('addID');
    this.strName = this.navParams.get('strName');
    this.coordinates.isHomePage = false;
    this.coordinates.showFooterOnSomePages = true;
  }

  doRefresh(refresher) {
    this.ionViewWillEnter();
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  ionViewWillEnter(){
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Buscando ítens...'});
    loading.present();
    var reg = {addid: "", ofeid: 0};
    reg.addid = this.addID;
    this.pesquisaProvider.showitems(reg)
    .subscribe(res => {
        this.items = res;
        loading.dismiss();
    }, error => {
        loading.dismiss();
        this.presentToast('Não foi possível exibir ítens de ' + this.strName);
    }); 
  }

  getStoreDetails(id, store){
    this.navCtrl.push(LojaPage, {strID: id, strName: store});
  }

  getProductDetails(id, store, starts){
    if (starts > 0){
      this.presentToast("Esta oferta ainda não está disponível!");
    }else{
      this.navCtrl.push(VerprodutoPage, {ofeID: id, strName: store});
    }
  }

  getDistance(lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - this.coordinates.latitude);  // deg2rad below
    var dLon = this.deg2rad(lon2 - this.coordinates.longitude); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(this.coordinates.latitude)) * Math.cos(this.deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    var distance = d.toFixed(1);
    var metric = (d < 1 ? (parseFloat(distance) * 1000) + " m": distance + " km");
    if ((this.coordinates.latitude != 0))
      return metric.replace(".", ",");
    else
      return 'calculando...'
  }

  deg2rad(deg) {
    return deg * (Math.PI/180)
  }

  presentToast(msgToast) {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 5000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }


}
