import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { GlobalVars } from '../../providers/globalVars';

@Component({
  selector: 'page-foto',
  templateUrl: 'foto.html',
})
export class FotoPage {

  prdSrv = "";
  arrImg = [];
  currentImg = "";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private globals: GlobalVars
    
  ) {
    this.prdSrv = this.navParams.get('prdSrv');
    this.arrImg = this.navParams.get('arrImg');
    this.currentImg = this.navParams.get('currentImg');
    this.globals.isHomePage = false;
    this.globals.showFooterOnSomePages = true;
  }

  dismiss() {
    let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(data);
  }


}
