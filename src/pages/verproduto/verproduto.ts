import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ToastController } from 'ionic-angular';
import { OfferProvider } from '../../providers/offer';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { SocialSharing } from '@ionic-native/social-sharing';
import { FotoPage } from './foto';
import { LaunchNavigatorOptions, LaunchNavigator } from '@ionic-native/launch-navigator';
import { GlobalVars } from '../../providers/globalVars';
import { Storage } from '@ionic/storage';
import { SearchProvider } from '../../providers/search';

@Component({
  selector: 'page-verproduto',
  templateUrl: 'verproduto.html',
})
export class VerprodutoPage {

    offers = [];
    items = [];
    arrCart = [];
    currStore = [];
    currQtd = [];
    currCarac = [];
    currVlUnit = [];
    numcarac: number = 0;
    ofestocks: string = "";
    bt_adic_upd: string = "ADICIONAR";
    ofeID: any;
    strName: string;
    storeAddress: string;
    classImgs: string;
    cartItems = {
      ofeid: 0,
      ctsid: 0,
      qtd: 0
    };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private toastCtrl: ToastController, 
    private pesquisaProvider: SearchProvider,
    private storage: Storage,
    public offerProvider: OfferProvider,
    public globals: GlobalVars,
    private socialSharing: SocialSharing,
    private loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private launchNavigator: LaunchNavigator
  ){
    this.ofeID = this.navParams.get('ofeID');
    this.strName = this.navParams.get('strName');
    this.globals.isHomePage = false;
    this.globals.showFooterOnSomePages = true;
  }

  addQtd(store: string, desc: string, carac: string, index: number, value: number = 0.00, stock: number){
    if (isNaN(this.currQtd[index])){
      this.currQtd[index] = 0;
      this.currCarac[index] = "";
      this.currStore[index] = "";
      this.currVlUnit[index] = 0;
    }
    if (this.currQtd[index] < stock){
      this.currQtd[index] += 1;
      this.currCarac[index] = carac;
      this.currStore[index] = store;
      this.currVlUnit[index] = value;
    }
    if (this.numcarac > 1){
      this.globals.addToCart(this.currStore, stock, desc, this.ofeID, this.currVlUnit, this.currQtd, this.currCarac);
    }
  }

  delQtd(store: string, desc: string, carac: string, index: number, value: number){
    this.currQtd[index] -= 1;
    if (isNaN(this.currQtd[index])){
      this.currQtd[index] = 0;
      this.currCarac[index] = "";
      this.currStore[index] = "";
      this.currVlUnit[index] = 0;
    }else{
      if (this.currQtd[index] < 1){
        this.currQtd[index] = 0;
        this.currCarac[index] = carac;
        this.currStore[index] = store;
        this.currVlUnit[index] = value;
      }
    }
    if (this.numcarac > 1){
      this.globals.addToCart(this.currStore, 0, desc, this.ofeID, this.currVlUnit, this.currQtd, this.currCarac);
    }
  }

  

  showPhotos(prodserv, arrPhotos, current) {
    let profileModal = this.modalCtrl.create(FotoPage, { prdSrv: prodserv, arrImg: arrPhotos, currentImg: current });
    profileModal.present();
  }

  getDistance(lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - this.globals.latitude);  // deg2rad below
    var dLon = this.deg2rad(lon2 - this.globals.longitude); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(this.globals.latitude)) * Math.cos(this.deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    var distance = d.toFixed(1);
    var metric = (d < 1 ? (parseFloat(distance) * 1000) + " m": distance + " km");
    if ((this.globals.latitude != 0))
      return metric.replace(".", ",");
    else
      return 'calculando...'
  }

  deg2rad(deg) {
    return deg * (Math.PI/180)
  }

  goToStore(){
    let options: LaunchNavigatorOptions = {
      appSelection: {
        dialogHeaderText: "Navegar até o local usando...",
        cancelButtonText: "Cancelar",
        rememberChoice: {
          enabled: false,
          prompt: {
            headerText: "Navegar até o local",
            bodyText: "Utilizar este App na próxima vez?",
            yesButtonText: "SIM",
            noButtonText: "NÃO"
          }
        }
    }};
    this.launchNavigator.navigate(this.storeAddress, options)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }

  updateCaracteristicItems(data: any){
    return this.storage.forEach( (value: string, key: string, iterationNumber: Number) => {
      if (value !== undefined){
        var only_cart_keys = key.split("-", 1);
        if (only_cart_keys[0] === "c"){
          var json = JSON.parse(JSON.stringify(value));
          data.forEach((element, key, i) => {
            if (element.ctsid == json.ctsid) {
              this.bt_adic_upd = "ATUALIZAR";
              this.currQtd[element.ctsid] = json.qtd;
              this.currStore[element.ctsid] = json.store;
              this.currCarac[element.ctsid] = json.caracteristic;
              this.currVlUnit[element.ctsid] = json.vlunit;
            }
          });
        }
      }
    });
  }

  ionViewWillEnter() {
    
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Por favor, aguarde...'});
    loading.present();
    this.offerProvider.getoffer( this.ofeID )
      .subscribe(data => {
        this.offers = data;
        this.numcarac = (data[0].caracteristics == null ? 0 : data[0].caracteristics.length);
        if (this.numcarac > 0){
          this.updateCaracteristicItems(data[0].caracteristics);
        }
        this.ofestocks = (data[0].caracteristics == null ? "" : "ou enquanto durar o estoque");
        this.storeAddress = data[0].address + ", " + data[0].number + " - CEP " + data[0].zipcode + " - " + data[0].neighboor + ", " + data[0].city +"/" + data[0].state;
        
        var reg = {addid: "", ofeid: this.ofeID};
        reg.addid = data[0].addid;
        this.pesquisaProvider.showitems(reg)
        .subscribe(res => {
            this.items = res;
        }, error => {
            
        }); 
        
        loading.dismiss();
      },
      error => { 
        loading.dismiss();
        this.presentToast("Não foi possível obter os dados da oferta.");
      });
  }

  getProductDetails(id, store, starts){
    if (starts > 0){
      this.presentToast("Esta oferta ainda não está disponível!");
    }else{
      this.navCtrl.push(VerprodutoPage, {ofeID: id, strName: store});
    }
  }

  share(store, prod, discount, image) {
    this.socialSharing.share('Se liga! ' + store + ' está oferecendo ' + prod + ' com ' + discount +'% de desconto, confira!', '', image, 'https://em2d.com.br/#seligaai')
  }

  /**
   * Method to show Toast messages 
   * on bottom of device
   * @param msgToast 
   */
  presentToast(msgToast) 
  {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 4500,
      position: 'bottom'
    });
    toast.present();
  }


}
