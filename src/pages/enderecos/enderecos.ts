import { Component, NgZone, ViewChild } from '@angular/core';
import { NavController, AlertController, NavParams, ToastController } from 'ionic-angular';
import { LoadingController } from '../../../node_modules/ionic-angular/components/loading/loading-controller';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AddressProvider } from '../../providers/address';
import { HomePage } from '../home/home';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { GlobalVars } from '../../providers/globalVars';

declare var google;

@Component({
  selector: 'page-enderecos',
  templateUrl: 'enderecos.html',
})
export class EnderecosPage {

  @ViewChild('mynumber') myNumber;
  @ViewChild('complement') myComplement;

  public newAddress: boolean = false;
  public form: FormGroup;

    autocompleteItems;
    autocomplete;
    debouncer: any;
    showitem: boolean = false;
    lenAddress: number = 0;
    currAddress: any = null;
    currNeighboor: any = null;
    addID: number = 0;
    uadID: number = 0;
    usr_address: string;
    usr_number: string;
    usr_complement: string;
    usr_neighboor: string;
    usr_zipcode: string;
    usr_city: string;
    usr_state: string;
    usr_tel1: string;
    usr_latitude: number;
    usr_longitude: number;
    usr_addtype: number = 0;
    submitAttempt: boolean = false;
    service = new google.maps.places.AutocompleteService();
    geocoder = new google.maps.Geocoder();

  constructor(public navCtrl: NavController,
              public viewCtrl: ViewController, 
              private zone: NgZone,
              public navParams: NavParams,
              private globals: GlobalVars,
              private loadingCtrl: LoadingController, 
              private addressProvider: AddressProvider,
              private formBuilder: FormBuilder,
              private toastCtrl: ToastController) {

                  this.globals.isHomePage = false;
                  this.globals.showFooterOnSomePages = false;

                  this.addID = this.navParams.get('addID');
                  this.uadID = this.addID;

                  this.autocompleteItems = [];
                  this.autocomplete = {
                    query: '',
                    number: '',
                    lati: '',
                    lngi: ''
                  };
                  
                  this.form = this.formBuilder.group({
                    address: ['', Validators.compose([Validators.minLength(5), Validators.maxLength(50), Validators.required])],
                    zipcode: ['', Validators.compose([Validators.maxLength(9), Validators.required])],
                    number: '',
                    neighboor: ['', Validators.compose([Validators.maxLength(40), Validators.required])],
                    city: ['', Validators.compose([Validators.maxLength(50), Validators.required])],
                    state: ['', Validators.compose([Validators.maxLength(2), Validators.minLength(2), Validators.required])],
                    complement: '',
                    tel1: ''
                  });
  }

  ionViewWillEnter(){
    if (this.uadID > 0)
    {
      this.myComplement.setFocus();
      let loading = this.loadingCtrl.create({
        spinner: 'crescent',
        content: 'Carregando endereço...'});
      loading.present();
      this.addressProvider.getaddress(this.uadID)
        .subscribe(data => {
          loading.dismiss();
          this.usr_address = data[0].address;
          this.usr_number = data[0].number;
          this.usr_complement = data[0].complement;
          this.usr_neighboor = data[0].neighboor;
          this.usr_zipcode = data[0].zipcode;
          this.usr_city = data[0].city;
          this.usr_state = data[0].state;
          this.usr_tel1 = data[0].tel1;
          this.usr_addtype = data[0].type;
          this.usr_latitude = data[0].latitude;
          this.usr_longitude = data[0].longitude;
        },
        error => { 
          loading.dismiss();
          this.presentToast("Não foi possível obter o endereço. Tente novamente mais tarde!");
          this.uadID = 0;
          this.addID = 0;
        });
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  chooseItem(terms: any) {
    this.getAddress(terms);
    this.showitem = false;
  }

  clear(){
      this.addID = 0;   
      this.form.controls["neighboor"].setValue("");
      this.form.controls["city"].setValue("");
      this.form.controls["state"].setValue("");
      this.form.controls["zipcode"].setValue("");
      this.form.controls["number"].setValue("");
      this.form.controls["complement"].setValue("");
      this.currAddress = null;
      this.currNeighboor = null; 
  }

  updateSearch() {
    if (this.addID == 0)
    {
      if (this.autocomplete.query.length < 6) {
        this.autocompleteItems = [];
        return;
      }else{
        if (this.currAddress == null){
          let me = this;
          me.showitem = true;
          this.service.getPlacePredictions({ input: this.autocomplete.query, types: ['geocode'], componentRestrictions: {country: 'BR'} }, function (predictions, status) {
            me.autocompleteItems = []; 
            me.zone.run(function () {
              if (predictions != null){
                predictions.forEach(function (prediction) {
                    me.autocompleteItems.push(prediction);
                });
              }
            });
          });
        }else{
          this.autocompleteItems = [];
          return;
        }
      }
    }
  }

  getAddress(terms) {
    let frm = this;
    if (terms.types[0] == "postal_code"){
      frm.form.controls["address"].setValue(terms.terms[0].value);
      frm.form.controls["zipcode"].setValue(terms.structured_formatting.main_text);
      frm.currAddress = terms.description;
      frm.currNeighboor = '';
    }else{
      frm.form.controls["address"].setValue(terms.structured_formatting.main_text);
      frm.currAddress = terms.structured_formatting.main_text;
      frm.currNeighboor = terms.structured_formatting .secondary_text;
    }
    frm.myNumber.setFocus();
  }

  getZipCode()
  {
    let frm = this;
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Atualizando...'});
    if (frm.autocomplete.number.length > 0)
    {
      loading.present();
      var address = '';
      var neighboor = '';
      var splitAddress = [];
      if (frm.currNeighboor == ''){
        splitAddress = frm.currAddress.split(" - ", 3);
        address = splitAddress[0];
        neighboor = splitAddress[1] + ' - ' + splitAddress[2];
      }else{
        address = frm.currAddress;
        neighboor = frm.currNeighboor;
      }
      frm.geocoder.geocode({'address': address + ', ' + frm.autocomplete.number + ' - ' + neighboor}, function(results, status) {
        if (status === 'OK') 
        {
          var lat = results[0].geometry.location.lat();
          var lng = results[0].geometry.location.lng();
          frm.autocomplete.lati = lat;
          frm.autocomplete.lngi = lng;
          let addressItems = results[0].address_components;
          for (var i = 0; i < addressItems.length; i++) {
            var addressType = results[0].address_components[i].types[0];
            if (addressType == "postal_code") {
              if (frm.currNeighboor != ''){
                frm.form.controls["zipcode"].setValue(results[0].address_components[i].long_name);
              }
            }
            if (addressType == "street_number") {
              frm.form.controls["number"].setValue(results[0].address_components[i].long_name);
            }
            if (addressType == "political") {
              frm.form.controls["neighboor"].setValue(results[0].address_components[i].long_name);
            }
            if (addressType == "administrative_area_level_1") {
              frm.form.controls["state"].setValue(results[0].address_components[i].short_name);
            }
            if (addressType == "administrative_area_level_2") {
              frm.form.controls["city"].setValue(results[0].address_components[i].long_name);
            }
            if (addressType == "route") {
              frm.form.controls["address"].setValue(results[0].address_components[i].long_name);
            } 
          }
          loading.dismiss();  

        } else {

          loading.dismiss(); 
          alert('Erro ao buscar o CEP do endereço. Motivo: ' + status);
        }
      });
    }
  }

  addAddress()
  {
    this.submitAttempt = true;
    let frm = this;
    if(frm.form.valid)
    {
      let loading = this.loadingCtrl.create({
        spinner: 'crescent',
        content: 'Por favor, aguarde...'});
      loading.present(); 

      if (this.uadID > 0)
      {

        if (frm.autocomplete.lati != '' || frm.autocomplete.lngi != ''){
          frm.form.value.latitude = frm.autocomplete.lati;
          frm.form.value.longitude = frm.autocomplete.lngi;
        }else{
          frm.form.value.latitude = this.usr_latitude;
          frm.form.value.longitude = this.usr_longitude;
        }
        frm.form.value.type = this.usr_addtype;
        frm.form.value.uadid = this.uadID;
        frm.addressProvider.updateAddress(frm.form.value).subscribe(res => {
          loading.dismiss();
          this.presentToast("Seu endereço foi alterado com sucesso!");
          frm.navCtrl.setRoot(HomePage);
        },erro => {
          loading.dismiss();
          this.presentToast("Não foi possível alterar seu endereço!");
        });

      }else{

        frm.form.value.latitude = frm.autocomplete.lati;
        frm.form.value.longitude = frm.autocomplete.lngi;
        frm.form.value.type = this.usr_addtype;
        frm.addressProvider.address(frm.form.value).subscribe(res => {
          loading.dismiss();
          this.presentToast("Seu endereço foi cadastrado com sucesso!");
          frm.navCtrl.setRoot(HomePage);
        },erro => {
          loading.dismiss();
          this.presentToast("Não foi possível salvar seu endereço!");
        });

      }
    }
  }

  updateToggleSet(whichToggle:number, ev){
    if(ev.checked === true){
      this.usr_addtype = 1;
    }else{
      this.usr_addtype = 0;
    }
  }

  getToggleSet(whichToggle:number){
    if (whichToggle == 1)
      return true;
    else
      return false;
  }

  presentToast(msgToast) {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 3000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

}
