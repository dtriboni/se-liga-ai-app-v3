import { Component } from '@angular/core';
import { NavController, AlertController, ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { LotteryProvider } from '../../providers/lottery';
import { HomePage } from '../home/home';
import { GlobalVars } from '../../providers/globalVars';

@Component({
  selector: 'page-sorteios',
  templateUrl: 'sorteios.html'
})
export class SorteiosPage {

   noitems: boolean = false;
   content: String = '';
   lotteryNumber: String = '';
   lotid: number;

  constructor(
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private globals: GlobalVars,
    private lotteryProvider: LotteryProvider, 
    public navCtrl: NavController) {

      this.globals.isHomePage = false;
      this.globals.showFooterOnSomePages = false;

    }

  ionViewWillEnter() {
     let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Buscando sorteios...'});
    loading.present();
    let user_info = JSON.parse(localStorage.getItem('login-info'));
    this.lotteryProvider.lottery(user_info.usrid).subscribe(data => {
        this.content = data.content;
        this.lotteryNumber = data.lastlotterynum;
        this.lotid = data.lotid;
        this.noitems = false;
        loading.dismiss();
    }, error => { 
        this.noitems = true;
        loading.dismiss();
        this.content = '';
        this.lotteryNumber = '';
        this.presentToast("Nenhum sorteio disponível no momento!");
    });
  }

  generateNumber(){
    let user_info = JSON.parse(localStorage.getItem('login-info'));
    var randNum = Math.floor(Math.random() * 79156637) + 988584338;

    this.presentToast("Anote seu número da sorte: " + user_info.usrid + "" + randNum);
    
    let loading = this.loadingCtrl.create({
        spinner: 'crescent',
        content: 'Salvando número...'});
    loading.present();
    
    var register = {lotid: this.lotid, email: user_info.email, number: user_info.usrid + "" + randNum};
    
    this.lotteryProvider.savelottery(register).subscribe(data => {
        loading.dismiss();
        this.navCtrl.setRoot(HomePage);
    }, error => { 
        loading.dismiss();
        this.presentToast("Não foi possível salvar seu número.");
    });      
  }

  presentToast(msgToast) {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 5000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

}
