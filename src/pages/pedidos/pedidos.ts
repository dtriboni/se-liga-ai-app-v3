import { Component } from '@angular/core';
import { NavController, LoadingController, NavParams, ToastController } from 'ionic-angular';
import { GlobalVars } from '../../providers/globalVars';
import { CartProvider } from '../../providers/cart';

@Component({
  selector: 'page-pedidos',
  templateUrl: 'pedidos.html'
})
export class PedidosPage {

  orders = [];
  noitems: boolean = false;
  strName: string;
  showOrderDetails = [];
  parseCategories: string = null;
  currIcon = [];
  currColor = [];

  constructor(private toastCtrl: ToastController, 
              public navCtrl: NavController, 
              public navParams: NavParams,
              public globals: GlobalVars,
              private loadingCtrl: LoadingController, 
              public cartProvider: CartProvider) 
  {
    this.globals.isHomePage = false;
    this.globals.showFooterOnSomePages = false;
  }

  ionViewWillEnter(){
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Buscando pedidos...'});
    loading.present();
    this.cartProvider.orders()
    .subscribe(res => {
      
      res.forEach(element => {
        this.currIcon[element.ordid] = "arrow-dropdown-circle";
        this.currColor[element.ordid] = "offers";
      });
      this.orders = res;
      this.noitems = false;
      loading.dismiss();
    
    }, error => {
       
        this.noitems = true;
        loading.dismiss();
       
    }); 
  }

  showHideOrder(item, arr){
    arr.forEach(element => {
      if(element.ordid != item){
        this.showOrderDetails[element.ordid] = false;
        this.currIcon[element.ordid] = "arrow-dropdown-circle";
        this.currColor[element.ordid] = "offers";
      }
    });
    if (this.showOrderDetails[item] == false || this.showOrderDetails[item] == null){
      this.showOrderDetails[item] = true;
      this.currIcon[item] = "arrow-dropup-circle";
      this.currColor[item] = "secondary";
      return true;
    }
    if (this.showOrderDetails[item] == true){
      this.showOrderDetails[item] = false;
      this.currIcon[item] = "arrow-dropdown-circle";
      this.currColor[item] = "offers";
      return true;
    }
    
  /*   
       */
    

     /*  var layerItemOrder = document.getElementById('items-' + item);
      
      layerItemArrowOrder.removeAttribute('name');
      layerItemArrowOrder.removeAttribute('color'); */
      // layerItemArrowOrder.setAttribute('name', 'arrow-dropup-circle');
      //layerItemArrowOrder.setAttribute('color', 'secondary'); /*
     /* layerItemOrder.classList.remove('hide-details');
      layerItemOrder.classList.add('show-details'); */
    //}else if (this.showOrderDetails == true){ 
      
     /*  var layerItemOrder = document.getElementById('items-' + item);
      var layerItemArrowOrder = document.getElementById('arrow-' + item);
      //layerItemArrowOrder.removeAttribute('name');
      //layerItemArrowOrder.removeAttribute('color');
      layerItemArrowOrder.setAttribute('name', 'arrow-dropdown-circle');
      layerItemArrowOrder.setAttribute('color', 'offers');
      layerItemOrder.classList.remove('show-details');
      layerItemOrder.classList.add('hide-details'); */
    //}
  }

  presentToast(msgToast) {
    let toast = this.toastCtrl.create({
      message: msgToast,
      duration: 5000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }


}
